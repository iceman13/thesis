(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#2d3743" "#ff4242" "#74af68" "#dbdb95" "#34cae2" "#008b8b" "#00ede1" "#e1e1e0"])
 '(column-number-mode t)
 '(custom-enabled-themes (quote (tango-dark)))
 '(desktop-save-mode t)
 '(display-time-mode t)
 '(fringe-mode 0 nil (fringe))
 '(global-auto-revert-mode t)
 '(global-hl-line-mode t)
 '(latex-run-command "latex")
 '(scroll-bar-mode nil)
 '(show-paren-mode t)
 '(tex-dvi-view-command
   (quote
    (cond
     ((eq window-system
	  (quote x))
      "xdvi -s 5")
     ((eq window-system
	  (quote w32))
      "yap")
     (t "dvi2tty * | cat -s"))))
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Ubuntu Mono" :foundry "DAMA" :slant normal :weight normal :height 143 :width normal)))))
(global-set-key (kbd "M-p") 'mode-line-other-buffer)
(add-hook 'latex-mode-hook 'turn-on-reftex)
(show-paren-mode 1)
