public class mcm{
	static int[]  p = {10, 20, 30, 40, 30};
	static int[][] m = new int[5][5], s = new int[5][5];
	
	static{
		for(int i = 0; i < 5; i++)
			for(int j = 0; j < 5; j++)
				m[i][j] = Integer.MAX_VALUE;
	}
	
	public static void main(String []args) {
		int ret = mcm(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
		System.out.println(ret);
	}
	
	private static int  mcm(int i, int j) {
		if( i == j) return (m[i][j] = 0);
		if(m[i] [j] < Integer.MAX_VALUE) return m[i][j];
		int k = i;
		while( k < j) {
			int q = mcm(i, k) + mcm(k+1, j) + p[i-1] * p[k] * p[j];
			if ( q < m[i][j]) {
			m[i][j] = q;
		}
		k++;
		}
		return m[i][j];
	}
}
