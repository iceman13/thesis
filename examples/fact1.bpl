var g: int;
procedure {:entrypoint} factorial(n: int) returns (r: int) modifies g;{
	if( n  <= 1) { r := 1;}
	else {
		if( n == 19) {
			if( g == -1) {
				call g := factorial(18);
				g := g * 19;
				r := g;
			} else {
				r := g;
			}
		} else {
			call r := factorial( n - 1);		
			r := n * r;
		}
	}
}
