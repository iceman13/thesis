class CatalnNumber {

    // A recursive function to find nth catalan number
    public static int[][] mem = new int[50][50];
    static {
	for(int i = 0; i < 10; i++)
	    for(int j = 0; j < 10; j++)
		mem[i][j] = -1;
    }
    

    int catalan(int n) {
        int res = 0;
        
        // Base case
        if (n <= 1) {
            return 1;
        }
        for (int i = 0; i < n; i++) {
	    if(mem[i][n-i-1] != -1)
		res += mem[i][n-i-1];
	    else {
		int c = catalan(i) * catalan(n - i - 1);
		res +=c;
		mem[i][n-i-1] = c;
	    }
        }
        return res;
    }

    public static void main(String[] args) {
        CatalnNumber cn = new CatalnNumber();
        for (int i = 0; i < 50; i++) {
            System.out.print(cn.catalan(i) + " ");
        }
    }
}
