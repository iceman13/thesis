---
title: Overview of proof of correctness of Impurity witness approach
author: Himanshu
theme: Copenhagen
---

# Building blocks
## Invariant : inv

The invariant is a formula which captures the values of the global
variables before and after each procedure call in a sequence of
procedure calls (history).

The invariant makes use of uninterpreted function symbols. An
invariant for procedure foo

###Example

$\mathit{inv := g = -1 \vee 19 * F(18)}$

## Path Condition : T

Given a program foo, we represent it in first order logic while :

1. covnerting foo to SSA.
2. replacing recursive procedure calls in foo with assignments to
uninterpreted fucntion symbols.

Note :

1.  This is a simplification of the rigirous notation used in the
    thesis.
2.  we assume that `T' magically restricts the value of the
global variable to inv, after each procedure call.


## Path Condition
### Example program
~~~~
int g = -1;
int factorial ( int n) {
  if( n <= 1) return 1;
  if( n == 19 && g == -1) {
    g = 19 * factorial( 18);
    return g;
  }
  if( n == 19 && g != -1)
    return g;
  return n * factorial ( n-1);
}
~~~~

## Path Condition
###  Path Condition for the given example

$ T := (n \le 1 \wedge ret = 1) \vee ( n = 19 \wedge g = -1 \wedge g1
= 19 * F(18) \wedge ret = g1) \vee ( n = 19 \wedge g \neq -1 \wedge
ret = g) \vee ( ret = n * F( n-1))$

