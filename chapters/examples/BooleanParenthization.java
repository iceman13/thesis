public class BooleanParenthization{
    static int[][] pos, neg;
    static boolean symbol[];
    static char operator[];
    
    public static void main(String[] args) {
	// symbols = { true, true, false, true};
	symbol = new boolean[4];
	symbol[0] = true;
	symbol[1] = true;
	symbol[2] = false;
	symbol[3] = true;
	
	
	// operators = { '|', '&', '^'};
	operator = new char[3];
	operator[0] = '|';
	operator[1] = '&';
	operator[2] = '^';
	pos = new int[4][4];
	neg = new int[4][4];
	for(int i = 0; i < 4; i++)
	    for(int j = 0; j < 4; j++) {
		pos[i][j] = -1;
		neg[i][j] = -1;
	    }

	System.out.println("pPar 0 1 :" + pPar(0, 3));
	// System.out.println("pPar 1 1 :" + pPar(1, 1));
	// System.out.println("pPar 0 0 :" + pPar(0, 0));	
    }
    
    public static int pPar(int start, int end) {
	if(pos[start][end] != -1) {
	    return pos[start][end];
	}
	if(start == end)
	    if(symbol[start]){
		return 1;
	    }else{
		return 0;
	    }
	int ret = -1;
	for(int k = start; k < end; k++) {
	    int a = 0;
	    int tStartK = pPar(start, k) + nPar(start, k);
	    int tKEnd = pPar(k+1, end) + nPar(k+1, end);
	    switch(operator[k]) {
	    case '&':
	        a = pPar(start, k) * pPar(k+1, end);
		break;
	    case '|':
		a = tStartK * tKEnd - nPar(start, k) * nPar(k+1, end);
		break;
	    case '^':
		a = pPar(start, k) * nPar(k+1, end) + nPar(start, k) * pPar(k+1, end);
	    }
	    ret = ret > a? ret: a;
	}
	return ret;
    }
    
    public static int nPar(int start, int end) {
	if(pos[start][end] != -1) {
	    return neg[start][end];
	}
	if(start == end)
	    if(!symbol[start])return 1;
	    else return 0;
	int ret = -1;
	for(int k = start; k < end; k++) {
	    int a = 0;
	    int tStartK = pPar(start, k) + nPar(start, k);
	    int tKEnd = pPar(k+1, end) + nPar(k+1, end);
	    switch(operator[k]) {
	    case '&':
	        a = tStartK * tKEnd - pPar(start, k) * pPar(k+1, end);
	        break;
	    case '|':
		a = nPar(start,k) * nPar(k+1, end);
		break;	
	    case '^':
		a = pPar(start, k) * nPar(k+1, end) + nPar(start, k) * pPar(k+1, end);
		break;
	    }
	    ret = ret > a ? ret: a;
	}
	return ret;
    }
}
