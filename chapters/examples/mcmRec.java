public class mcmRec{
	static int[]  p = {10, 20, 30, 40, 30};
	static int[][] m = new int[5][5], s = new int[5][5];
	
	static{
		for(int i = 0; i < 5; i++)
			for(int j = 0; j < 5; j++)
				m[i][j] = Integer.MAX_VALUE;
	}
	
	public static void main(String []args) {
		int ret = mcmRec(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
		System.out.println(ret);
	}
	private static int  mcmRec(int i, int j) {
		if( i == j) return (m[i][j] = 0);
		if(m[i] [j] < Integer.MAX_VALUE) return m[i][j];
		int k = i;
		m[i][j] = foo(i, j, k, m[i][j]);
		return m[i][j];
	}

	private static int foo(int i, int j, int k, int min) {
		int q;
		if(k >= j) {
			return min;
		} else {
		q = mcmRec(i, k) + mcmRec(k + 1, j) + p[i -1] * p[k] * p[j];
		if( q < min ) min = q;
			return foo(i, j, k+1, min);
		}
	}
}
