import java.io.*;
 
class PartitionRecursive {
 
    // Returns true if arr[] can be partitioned in two subsets of
    // equal sum, otherwise false
    static boolean findPartition (int arr[], int n, int sum)
    {
        // Fill the partition table in botton up manner
        for (i = 1; i <= sum; i++)
        {
            for (j = 1; j <= n; j++)
            {
                part[i][j] = part[i][j-1];
                if (i >= arr[j-1])
                    part[i][j] = part[i][j] ||
                                 part[i - arr[j-1]][j-1];
            }
        }
 
        // uncomment this part to print table
        for (i = 0; i <= sum/2; i++)
        {
            for (j = 0; j <= n; j++)
                // System.out.printf ("%4d", part[i][j]);
		System.out.print(part[i][j] + "\t");
            System.out.printf("\n");
        }
 
        return part[sum/2][n];
    }
 
    /*Driver function to check for above function*/
    public static void main (String[] args)
    {
        int arr[] = { 2, 2, 1, 3, 1, 1};
        int n = arr.length;
	int sum = 0;
        int i, j;
        for (i = 0; i < n; i++)  // calculate sum
            sum += arr[i];
	boolean part[][]=new boolean[sum/2+1][n+1];
        for (i = 0; i <= n; i++)  // initialize top row as true
            part[0][i] = true;
        for (i = 1; i <= sum/2; i++)  // initialize leftmost column, except part[0][0], as 0
            part[i][0] = false;
 

        if (findPartition(arr, n, sum/2) == true)
            System.out.println("Can be divided into two " +
                               "subsets of equal sum");
        else
            System.out.println("Can not be divided into" +
			       " two subsets of equal sum");
	
    }
}
/* This code is contributed by Devesh Agrawal */
