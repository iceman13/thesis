\documentclass[../main.tex]{subfiles}
\begin{document}
\chapter{Algorithms (Recursion Free)} \label{cha:algoRecFree}
In this chapter we present the algorithms that check if the procedure
$\foo$ is weak observationally pure under invariant $\inv$. In this
chapter we will present the routines that work only on recursion-free
procedures. First we describe how we represent the programs in logic
so that it can be fed to an smt solver, then we present the checkers
which generate formulae which encode if the method is observationally
pure. For now we assume that $\inv$ is an inductive invariant, we
present routines to check inductivity and building an invariant in
Chapter~\ref{cha:algo}.

\section{Path Condition Generation for recursion free procedures}
\label{sec:pcGenForRecFree}
Here we describe how to generate the path condition for program $\foo$
in the format as described in Section~\ref{sec:programStructure}.

Since $\foo$ does not have recursion or loops it can have only a
finite number of path for the beginning of $\foo$ until its end. For
each such path we conjunct all the statements to get a formula
$\formula$ which encodes the execution of that path. Next we disjunct
such formulae $\formula$ for all paths in $\foo$ to get a formula
$\formula'$. Next we universally quantify all the variables in
$\formula'$ except $\g, \gout,\n \; and \; \retVar$, we call this formula
path condition of $\foo$. We use notation $\pathCondition$ to
represent the path condition of $\foo$. Here $\g$ and $\gout$ represent the
value of the global variable $\g$ in the beginning and end of $\foo$
respectively. $\n$ and $\retVar$ represent the value of the parameter
and the return value respectively.

Note: In Chapter~\ref{cha:algo} we define path condition for programs
with recursion and we use the same notation $\pathCondition$ there as
well, although they have different free variables.

\section{Observational purity Checkers (for recursion free procedures)}
In this section we present routines that take the path condition of
$\foo$, $\pathCondition$ and an inductive invariant $\inv$ and return
a formula that encodes whether $\foo$ is Observationally pure. One
checker is called Existential checker, it constrains definition of
$\foo$ such that it's return value respects a function.  Then it is
the task of the SMT solver to check if such a function exists, hence
it is called the existential checker. The other checker defines the
behaviour that should not exist if $\foo$ is a function, hence it is
called the impurity witness.  These routines parse the program once to
generate these formulae, and always terminate.

\subsection{Existential Checker (recursion free)} \label{sec:existentialCheckerRecFree}
The Existential checker (Algorithm~\ref{algo:someOPcheckRecFree})
gives a formula which if satisfiable implies $\foo$ is weak
observationally pure (proved in Chapter~\ref{cha:proof}).

In the formula encoded by Algorithm~\ref{algo:someOPcheckRecFree}, the
return value is restricted to $\F(\n)$ while the incoming global state
is a good state. This formula has $\F$ as the only free variable. Thus
if this formula has a satisfying assignment, it implies that $\F$ is
exactly the mathematical function that $\foo$ implements.

\begin{figure}[htp]
  \begin{algorithm}[H]
    \begin{align*}
      \OPCheckE_{recursion \mhyphen free} & \mi{(\inv :
      invariant, T : path \; condition)} \equiv \\
      & (\forall \g) (\forall \n) (\forall \retVar) (\forall \gout) \\
      & \{\inv \wedge \pathCondition\\
      &\implies \retVar = \F(\n) \} \\
    \end{align*}
    \caption{Existential check (recursion free) : produces a formula
      whose satisfiability implies there is a consistent
      interpretation}
    \label{algo:someOPcheckRecFree}
  \end{algorithm}  
\end{figure}

\subsection{Impurity witness (recursion free)}
The Impurity witness (Algorithm~\ref{algo:pairwiseOPCheckRecFree})
produces a formula which if unsatisfiable implies $\foo$ is weak
observationally pure. The algorithm takes as input the verification
condition and the invariant $\inv$.

In the formula encoded by Algorithm~\ref{algo:pairwiseOPCheckRecFree},
the return values from all pairs of executions of $\foo$ with the same
parameter value are compared to see if they can be unequal, assuming
both runs start from good states. Thus if
the returned formula is unsatisfiable then there exist no two
executions of $\foo$ that begin in a good state and return a different
result.

The formula returned by this checker describes a witness of
non-observational purity, hence the returned formula must be UNSAT for
$\foo$ to be observationally pure. Whereas, the formula returned by
Algorithm~\ref{algo:someOPcheckRecFree} describes the case where $\foo$
is observationally pure and equivalent to function $\F$. Thus the
different treatment for both the formulae.

\begin{figure}[htp]
  \begin{algorithm}[H]
    \begin{align*}
      \OPCheckA_{recursion \mhyphen free} &\mi{(\inv : invariant,
      T: path\; condition)} \equiv \\
      &\inv[\g_1/\g] \wedge \inv[\g_2/\g]\\
      &\wedge \n_1 = \n_2 \\
      &\wedge \pathCondition[\g_1/\g, \n_1/\n, \retVar_1/\retVar,
       \gout_1/\gout]\\
      &\wedge \pathCondition[\g_2/\g, \n_2/\n, \retVar_2/\retVar,
       \gout_2/\gout]\\
      &\wedge \retVar_1 \neq \retVar_2\\
    \end{align*}
    \caption{Impurity witness (recursion free) : produces a formula
      whose unsatisfiability implies weak observational purity}
    \label{algo:pairwiseOPCheckRecFree}
  \end{algorithm}  
\end{figure}

\subsection{Comparing the two checkers}
Existential approach is more precise than the impurity witness approach
because in principle the impurity witness approach is looking for
instances of observationally impure behaviour whereas the existential
approach is trying to prove a procedure observationally pure. For
instance procedure $bar$ in Figure~\ref{fig:comparison} for the
invariant $\g = \F(0) \vee \g = \F(1)$ is marked observationally pure
by the existential approach whereas is marked not observationally pure
by the impurity witness approach.

\begin{figure}
\begin{verbatim}
int bar(int n) { 
  return g; 
}
\end{verbatim}
\caption{An example to illustrate that existential approach is more
precise that the impurity witness approach}
\label{fig:comparison}
\end{figure}

The impurity witness although less precise produces formulae which are
easier for the SMT solver. Empirically we found that in most cases the
SMT solver does not terminate on formulae produced by the existential
approach.
\end{document}
