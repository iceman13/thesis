\documentclass[../thesis.tex]{subfiles}
\begin{document}
\chapter{Algorithm} \label{cha:algo}
In this chapter we first define an invariant which is used to
represent the set of global states that are reachable in a history of
procedure `\foo'. Then we given an overview of our approach. Next, we
define a logical formula, which we call \textit{path condition} that
captures all the behaviors of procedure `\foo'. Then, using the path
condition, we describe two techniques to check if `\foo' is
observationally pure. Finally, we describe a technique to build the
invariant $\inv$ for procedure `\foo' from a given initial global
state.

%% Then we present the algorithms that check if the
%% procedure `\foo' is observationally pure under invariant
%% $\inv$.
%% Procedure `\foo' is observationally pure under invariant
%% $\inv$ if for all traces in a histories that begins in a global state
%% $\gstate$, such that $(\gstate, \F) \satisfies \inv$, respect each
%% other where $\F$ represents the mathematical function which `\foo'
%% emulates. This definition is intentionally cyclic because if `\foo'
%% emulates mathematical function $\F$ then there is no need to check if
%% it is observationally pure.

\section{Invariant}
In general an invariant is a condition that can be relied upon to be
true during execution of the program~\cite{wiki:invariant}. There is a
large body of work on loop invariants ( few of them are
\cite{floyd1993assigning, puasuareanu2004verification,
  sankaranarayanan2004non}). In these works, a sub-set of variables in
the program serve as free variables in the formula representing the
invariant. Thus, the invariant gives a set of states that the program
can be in, at a given program point or set of program points. We
propose using a syntactic invariant $\inv$ for checking observational
purity, where $\inv$ is also a semantic invariant only if the given
procedure `\foo' is observationally pure. The invariant inherently
assumes that the given procedure `\foo' is observationally pure.

Our invariant has an extra free variable, a mathematical function
symbol $\F$. The given procedure `\foo' is assumed to respect the
mathematical function $\F$. Thus the invariant construction assumes
that procedure `\foo' is observationally pure and later the same
invariant is used to prove that procedure `\foo' is observationally
pure. This technique seems to be cyclic, but in
Chapter~\ref{cha:proof} prove that this technique is sound.

\begin{definition}[invariant]
  An invariant is a formula with function symbol $\F$ and the global
  variables of `\foo' as free variables. We typically use an invariant
  to represent the set of global states that arise before and after
  each procedure call of the procedure `\foo' in a history or set of
  histories. Such an invariant has only the global variables as free
  variables in the formula. The function symbol $\F$ is assumed to
  represent procedure `\foo'.
  
  Let $\gstate$ be a global state, and $\F'$ be a function, and $\inv$ be
  a invariant. If we substitute $\gstate$ for $\g$ and $\F'$ for $\F$
  in $\inv$, if $\inv[\gstate/\g, \F'/\F]$ is true then, we say
  $(\gstate, \F') \satisfies \inv$. For instance let $\inv = \g = -1
  \vee \g = \F(18) * 19$ then $(0, \lambda n. n!) \satisfies \inv$.
  
  Note : we say a global state $\gstate$ is a good state if $(\gstate,
  \F) \satisfies \inv$, where $\F$ is the mathematical function that
  is assumed to represent procedure `\foo'.
\end{definition}

\section{Overview}
%% Our approach has a cyclic component, in the sense that we assume what
%% we are proving. The invariant we use gives the set of global states
%% that may be encountered on procedure boundaries in all histories.
%% Our invariant assumes that the given procedure is observationally
%% pure. Next, we use this invariant to prove the given procedure to be
%% observationally pure. Thus the cycle. 

%% First we build an invariant $\inv$, which is formula with free
%% variables $\F$ and $\g$. The variable $\F$ is a mathematical function
%% and procedure `\foo' is assumed to emulate $\F$ during the
%% construction of invariant $\inv$. At this point the invariant $\inv$
%% is a pseudo-invariant, because of the underlying assumption that
%% procedure `\foo' is observationally pure. It is an invariant in the
%% true sense only when `\foo' is confirmed to be observationally pure.

We present two observational purity checkers in this thesis. These
checkers take an invariant and a representation of the program and
then produce a formula whose satisfiability or unsatisfiability
(depending upon the observational purity checker used) decides whether
`\foo' is observationally pure.

In this chapter we first preset the technique we use to represent the
given procedure in logic. Then we present the two checkers and then
finally we present a technique to produce an invariant for a given
procedure.
\section{Path condition generation}
The path condition $\pathCondition$ for procedure `\foo' is a formula
that represents the procedure `\foo' in logic.  $\pathCondition$ has
the following set of free variables \{$\g, \gout,\n, \; \retVar,
\gbef_1, \cdots \gbef_m, \gaft_1, \\\cdots \gaft_m$\}. Variable $\g$
represents the value of the global variable `g' at the beginning of
procedure `\foo' whereas $\gout$ represents value of `g' at the end of
`\foo'. Variable $\n$ represents the value of the argument, $\retVar$ the
return value. And $\gbef_i$ and $\gaft_i$ represent the value of `g'
before and after the $i^{th}$ procedure call statement and there are
$m$ procedure calls in `foo'.

In Chapter~\ref{cha:pLanguage} we described the structure of the
expected input to our algorithms and how to convert a program to this
format. Assuming the program is in the format as described in
Chapter~\ref{cha:pLanguage}. To generate a formula representing the
program we use the function R in Figure~\ref{fig:pcConverter}. Next we universally
quantify all the variables except \{$\g, \gout,\n, \retVar, \gbef_i,
\gaft_i$\} where $0<i\leq m$ to get $\pathCondition$.  For
e.g. Figure~\ref{fig:factorialPathCondition} gives the path condition
$\pathConditionFact$ for procedure `factorial' in
Listing~\ref{lst:factorialSSAFinal}.

\begin{figure}
  \begin{align*}
    \pathConditionFact :=& (\forall temp) (\forall temp1) \{ n \leq 1
    \wedge \retVar = 1 \wedge \gout = g\\
    \vee& \neg (n \leq 1) \wedge (g = -1 \wedge n = 19) \wedge \gbef1 =
    g \wedge\\
    &\mi{temp} = \F(18) \wedge \gaft1 = g1 \wedge g2 = 19 *
    \mi{temp} \wedge \retVar = g2 \wedge \gout = g2\\
    \vee& \neg(n \leq 1) \wedge \neg(g = -1 \wedge n = 19) \wedge (g
    \neq -1 \wedge n = 19) \wedge \retVar =g \wedge \gout = \g \\
    \vee& \neg(n \leq 1) \wedge \neg(g = -1 \wedge n = 19) \wedge
    \neg(g \neq -1 \wedge n = 19) \wedge \gbef2 = g \wedge \\ 
    &\mi{temp2} = \F(n -1) \wedge \gaft2 = g1 \wedge \retVar = n *
    \mi{temp2} \wedge \gout = g1\}\\
  \end{align*}
  \caption{Path condition for procedure `factorial' in
    Listing~\ref{lst:factorialSSAFinal} generated using `R' in
    Figure~\ref{fig:pcConverter}} 
  \label{fig:factorialPathCondition}
\end{figure}

In Figure~\ref{fig:pcConverter} we define a function that converts the
given procedure `\foo' to logic. It assumes that the given procedure
is in the format as described in Chapter~\ref{cha:pLanguage}. After
this conversion we universally quantify all the variables except
\{$\g, \gout,\n, \; \retVar, \gbef_i, \gaft_i$\} where $0<i\leq m$.

\begin{figure}
  \begin{align*}
    R(\statement_1;\statement_2) ::=& R(\statement_1) \wedge
    R(\statement_2)\\
    R(assume(e)) ::=& e\\
    R(havoc(e)) ::=& true\\
    R(if(e)\; \statement_1\; else\; \statement_2) ::=& e \wedge R(\statement_1)
    \vee \neg e \wedge R(\statement_2)\\
    R(ident_1 = ident_2) ::=& ident_1 = ident_2\\
    R(ident_1 = \F(ident_2)) ::=& ident_1 = \F(ident_2)\\
    R(ident = logical\mhyphen expr ::= & ident = logical\mhyphen expr\\
    R(ident = arithmetic\mhyphen expr) ::= & ident = arithmetic\mhyphen expr\\
  \end{align*}
  \caption{Function R: converts a given procedure to a path condition}
  \label{fig:pcConverter}
\end{figure}



%% Path condition generation for procedures that have recursive calls is
%% very similar to path condition generation for recursion free
%% procedures as described in Section~\ref{sec:pcGenForRecFree}. The only
%% difference is the set of free variables. We add the variables $\gbef_i
%% \; and \; \gaft_i$ for the $i^{th}$ recursive call. $\gbef_i \; and \;
%% \gaft_i$ represent the value of latest version of $\g$ before and
%% after the $i^{th}$ recursive call respectively.

%% The reason we have these extra free variables is that
%% \begin{enumerate}
%% \item before each recursive call, the global state must be a good
%%   state.
%% \item after each recursive call, the global state must be a good
%%   state.
%% \end{enumerate}
%% Hence we have these extra free variables and they are used in the
%% invariant generation and inductivity checker.

\section{Observational purity checkers}
In this section we present routines that take the path condition of
procedure `\foo', i.e., $\pathCondition$, and the given invariant
$\inv$, and return a formula that encodes whether `\foo' is
observationally pure. The first checker is \emph{existential
checker}. It produces a formula that is satisfiable only if `\foo' is
observationally pure. The other checker is the \emph{impurity witness
checker}. Impurity witness checker produces a formula that describes
an instance of behaviour where `\foo' is not observationally pure. If
this formula is unsatisfiable, `\foo' is observationally pure.

\subsection{Existential Checker}
The algorithm for the existential checker is presented in
Algorithm~\ref{algo:someOPcheckCombined}.  Let the formula encoded by
Algorithm~\ref{algo:someOPcheckCombined} be $\formula_{existential}$
for procedure `\foo' and invariant $\inv$. Formula
$\formula_{existential}$ has a single free variable, $\F$ and its
domain is mathematical functions with the same signature as procedure
`\foo'. The mathematical function $\F$ represents procedure `\foo' in
the formula.

In formula $\formula_{existential}$, the value of the global variable
$\g$ is constrained to satisfy the invariant $\inv$ at the beginning
of the procedure `\foo' as well as after the procedure call statement
(see line no (4) and (5), in Algorithm~\ref{algo:someOPcheckCombined}
respectively). Next, the return values is constrained to respect
function $\F$ (line no (6), Algorithm~\ref{algo:someOPcheckCombined}).
Let the global state in procedure `\foo' at the beginning, before the
procedure call, after the procedure call and at the end of `\foo' be
$\gstate_{beg}, \gstate_{bef}, \gstate_{aft}, \gstate_{end}$
respectively. Then the invariant must be such if $(\gstate_{beg}, \F)
\satisfies \inv$ then $(\gstate_{bef}, \F) \satisfies \inv$, otherwise
for the inner trace the starting global state would not satisfy the
invariant. Similarly if $(\gstate_{aft}, \F) \satisfies \inv$ then
$(\gstate_{end}, \F) \satisfies \inv$, otherwise for the outer trace
wrt the current trace the state after the procedure call statement would not
satisfy the invariant. Thus we add the constraints on the global
state at the end of procedure `\foo' and before the procedure call
(see line no (6) and (7),
Algorithm~\ref{algo:someOPcheckCombined}). Theses checks on
$\gstate_{end}$ and $\gstate_{bef}$ ensure the invariant is valid.


\begin{figure}[htp]
  \begin{algorithm}[H]
    \begin{align*}
      \OPCheckE & \mi{(\inv :
        invariant, \pathCondition : path \; condition)} \equiv \\
       (\forall & \g) (\forall \n) (\forall \retVar) (\forall \gout) &(1)\\
       (\forall & \gbef_1) (\forall \gbef_2) \cdots(\forall \gbef_m) &(2)\\
       (\forall & \gaft_1) (\forall \gaft_2) \cdots(\forall \gaft_m)
      \{& (3)\\
      & \inv \wedge \pathCondition \wedge & (4)\\
      & \inv[\gaft_1/\g] \wedge \inv[\gaft_2/\g] \cdots \wedge
      \inv[\gaft_m/\g] & (5)\\
      & \implies (\retVar = \F(\n) \wedge \inv[\gout/\g] \wedge & (6) \\
      & \inv[\gbef_1/\g] \wedge \inv[\gbef_2/\g] \cdots \wedge
      \inv[\gbef_m/\g]) & (7) \\
       \}&\\
    \end{align*}
    \caption{Existential check : produces a formula whose
      satisfiability implies the given procedure is observationally
      pure} 
    \label{algo:someOPcheckCombined}
  \end{algorithm}  
\end{figure}

\begin{figure}
  \begin{align*}
    %% \OPCheckE & \mi{(\g = -1 \vee \g  = 19 * \F(18) :
    %%   invariant, \pathConditionFact : path \; condition)} \equiv \\
    (\forall& \g) (\forall \n) (\forall \retVar) (\forall \gout)\\
    (\forall& \gbef_1) (\forall \gbef_2) \\
    (\forall& \gaft_1) (\forall \gaft_2) \{\\
    &(g = -1 \vee \g  = 19 * \F(18)) \wedge \pathConditionFact \wedge\\
    &(g = -1 \vee \g  = 19 * \F(18))[\gaft_1/\g] \wedge\\
    &(\g = -1 \vee \g  = 19 * \F(18))[\gaft_2/\g] \\
    &\implies \{\retVar = \F(\n) \wedge (\g = -1 \vee \g  = 19 * \F(18))[\gout/\g] \wedge\\
    &(\g = -1 \vee \g  = 19 * \F(18))[\gbef_1/\g] \wedge (\g = -1 \vee \g  = 19 * \F(18))[\gbef_2/\g])\}\\
    \}&\\
  \end{align*}
  \caption{Formula generated using Existential approach for procedure
    `factorial' in Listing~\ref{lst:factorial}: if this formula is
    satisfiable then procedure `factorial' is observationally pure.
    In this formula we have used the invariant as $(\g = -1 \vee \g = 19
    * \F(18))$}
  \label{fig:existentialFactorialComplete}
\end{figure}

In Figure~\ref{fig:existentialFactorialComplete} we present the formula
produced by the existential approach for procedure `factorial' in
Listing~\ref{lst:factorial}.

\subsection{Impurity witness} \label{sec:impurityWitness}
The algorithm for the impurity witness checker is presented in
Algorithm~\ref{algo:pairwiseOPcheckCombined}. Let the formula encoded
by Algorithm~\ref{algo:pairwiseOPcheckCombined} be
$\formula_{impurity}$ for procedure `\foo' and invariant
$\inv$. Formula $\formula_{impurity}$ has free variables $\{\g, \gout,\n, \retVar,
\gbef_1, \cdots \gbef_m, \gaft_1, \cdots \gaft_m, \F\}$ where $m$ is
the number of procedure calls in the given procedure. And same as the
existential checker mentioned above $\F$ has the same signature as
procedure `\foo'.

In this approach we compare all pairs of traces the given procedure
`\foo' that begin with the same argument value. Then, search an
instance where the two traces return different results. Thus,
identifying a witness of impurity. In
Algorithm~\ref{algo:pairwiseOPcheckCombined}, we compare two instances
of $\pathCondition$, for one instance we augment all its free
variables with sub script $\alpha$ and for the other we use
$\beta$. First, we restrict the value of $\g$ at the beginning of the
procedure `\foo' (see line no. (1)). Next, we restrict the values of
$\g$ after the procedure call statement to satisfy the invariant (see
line no. (2) and (3)). Then, we equate the argument values for both
the instances of $\pathCondition$ (see line no. (4)). Next, we have
the two path condition of procedure `\foo' with the appropriate
renaming of their free variables (see line no (5) and (6)). Next, the
return value from both the path conditions are marked for inequality
(see line no. (7)). Finally, similar to the existential approach above
we check if values of global variable $\g$ satisfy the invariant
before the procedure call and at the end of procedure `\foo' (see line
no. (7) and (8)). Note that it is sufficient to check if the invariant
satisfies for one instance of the path condition.

\begin{figure}[htp]
  \begin{algorithm}[H]
    \begin{align*}
      \OPCheckA &\mi{(\inv : invariant,
        \pathCondition: path\; condition)} \equiv &\\
      &\inv[\g_\alpha/\g] \wedge \inv[\g_\beta/\g]  & (1)\\
      & \wedge \inv[\gaft_{\alpha1}/\g] \wedge
      \inv[\gaft_{\alpha2}/\g] \cdots \wedge \inv[\gaft_{\alpha m}/\g]
      & (2)\\
      & \wedge \inv[\gaft_{\beta1}/\g] \wedge \inv[\gaft_{\beta2}/\g]
      \cdots \wedge \inv[\gaft_{\beta m}/\g] & (3)\\
      &\wedge \n_\alpha = \n_\beta  & (4)\\
      &\wedge \pathCondition[\g_\alpha/\g, \n_\alpha/\n, \retVar_\alpha/\retVar,
        \gout_\alpha/\gout, & (5_a)\\
        &\gbef_{\alpha1}/\gbef_1 \cdots, \gaft_{\alpha 1}/\gaft_1, \cdots
        \gaft_{\alpha m}/\gaft_m]  & (5_b)\\
      &\wedge \pathCondition[\g_\beta/\g, \n_\beta/\n, \retVar_\beta/\retVar,
        \gout_\beta/\gout, & (6_a)\\
        &\gbef_{\beta1}/\gbef_1 \cdots, \gaft_{\beta 1}/\gaft_1, \cdots
        \gaft_{\beta m}/\gaft_m] & (6_b)\\
      &\wedge (\retVar_\alpha \neq \retVar_\beta \vee \neg\inv[\gbef_{\alpha1}/\g] \vee
       \cdots \neg\inv[\gbef_{\alpha m}/\g] & (7) \\
        &\vee \neg\inv[\gout_\alpha/\g])  & (8)\\
    \end{align*}
    \caption{Impurity witness : produces a formula whose unsatisfiability
    implies observational purity.}
    \label{algo:pairwiseOPcheckCombined}
  \end{algorithm}  
\end{figure}

\begin{figure}
  \begin{align*}
    &(\g = -1 \vee g = \F(18) * 19)[\g_\alpha/\g] \wedge (\g = -1 \vee
    g = \F(18) * 19)[\g_\beta/\g] \wedge  & (1)\\
    & (\g = -1 \vee g = \F(18) * 19)[\gaft_{\alpha1}/\g] \wedge
    & (2)\\
    & (\g = -1 \vee g = \F(18) * 19)[\gaft_{\alpha2}/\g] \wedge
    & (3)\\
    & (\g = -1 \vee g = \F(18) * 19)[\gaft_{\beta1}/\g] \wedge & (4)\\
    & (\g = -1 \vee g = \F(18) * 19)[\gaft_{\beta2}/\g] \wedge & (5)\\
    &\wedge \n_\alpha = \n_\beta  & (6)\\
    &\wedge \pathCondition[\g_\alpha/\g, \n_\alpha/\n, \retVar_\alpha/\retVar,
      \gout_\alpha/\gout, \gbef_{\alpha 1}/\gbef_1, \gaft_{\alpha 1}/\gaft_1]  & (7)\\
    &\wedge \pathCondition[\g_\beta/\g, \n_\beta/\n, \retVar_\beta/\retVar,
      \gout_\beta/\gout, \gbef_{\beta1}/\gbef_1, \gaft_{\alpha 1}/\gaft_1] & (8)\\
    &\wedge (\retVar_\alpha \neq \retVar_\beta \vee \neg(\g = -1 \vee
    g = \F(18) * 19)[\gbef_{\alpha1}/\g] & (9) \\
    &\vee \neg(\g = -1 \vee g = \F(18) * 19)[\gout_\alpha/\g])  & (10)\\
  \end{align*}
    \caption{Formula generated using impurity witness approach for procedure
    `factorial' in Listing~\ref{lst:factorial}: if this formula is
    unsatisfiable then procedure `factorial' is observationally pure.
    In this formula we have used the invariant as $(\g = -1 \vee \g = 19
    * \F(18))$}
  \label{fig:impurityWitnessFactorialComplete}
\end{figure}

In Figure ~\ref{fig:impurityWitnessFactorialComplete} we present the formula
generated by the impurity witness approach for procedure `factorial' in
Listing~\ref{lst:factorial}. 

\subsection{Comparison of impurity witness and existential checkers}
The Existential approach is more precise than the impurity witness
approach (see Chapter~\ref{cha:proof}) because the existential
approach is an optimistic approach whereas the impurity witness is a
pessimistic approach. The existential approach intuitively poses the
question, is there a satisfying mathematical function that is
respected by the given procedure. Whereas, the impurity witness
approach represents the question, is the given procedure mimics a
mathematical function, no matter what. The existential approach can be
used to list the mathematical functions that may be equivalent to the
given procedure.



%% because in principle the impurity witness approach is looking for
%% instances of observationally impure behaviour whereas the existential
%% approach is trying to prove a procedure observationally pure. For
%% instance procedure $bar$ in Figure~\ref{lst:comparison} for the
%% invariant $\g = \F(0) \vee \g = \F(1)$ is marked observationally pure
%% by the existential approach whereas is marked not observationally pure
%% by the impurity witness approach.

\begin{lstlisting}[caption={Procedure `bar': illustrates that
      existential approach is more precise that the impurity witness
      approach.}, label=lst:comparison]
int bar(int n) { 
  return g; 
}
\end{lstlisting}

\begin{figure}
  \begin{minipage}[t]{0.5\textwidth}
    \begin{align*}
      &(\g = \F(0) \vee \g = \F(1))[\g_\alpha/\g] \wedge\\
      &(\g = \F(0) \vee \g = \F(1))[\g_\beta/\g] \wedge\\
      &\n_\alpha = \n_\beta \wedge\\
      &\retVar_\alpha = g_\alpha \wedge \retVar_\beta = \g_\beta \wedge\\
      &\retVar_\alpha \neq \retVar_\beta\\
      &\text{(Impurity witness approach)}
    \end{align*}
  \end{minipage}
  \begin{minipage}[t]{0.5\textwidth}
    \begin{align*}
      &(\g = \F(0) \vee \g = \F(1))\wedge \\
      &\retVar = \g\wedge \\
      &\retVar = \F(n)\wedge \\ \\ \\
      &\text{(Existential approach)}
    \end{align*}
  \end{minipage}
  \caption{Formulae produced by the two checkers for procedure `bar' in
    Listing~\ref{lst:comparison}}
  \label{fig:comparison}
\end{figure}

Procedure `bar' in Listing~\ref{lst:comparison} for the invariant $(\g
= \F(0) \vee \g = \F(1))$ is observationally pure for all mathematical
functions such that $\forall n.\F(0) = \F(1) = \F(n)$, (thus it must be
a constant function). And there are many satisfying assignments to
this formula.
In
Figure~\ref{fig:comparison} we give the formulae produced for
procedure `bar' by the two approaches. Now, the empirical approach
marks `bar' observationally pure whereas the impurity witness approach
marks it as observationally impure.

The impurity witness although less precise, produces formulae which are
easier for the SMT solver. Empirically we found that in most cases the
SMT solver does not terminate on formulae produced by the existential
approach.

%% \section{Inductivity Check}
%% The inductivity check in Algorithm~\ref{algo:inductivityCheck} returns
%% a formula that if valid implies that every execution of $\foo$ that
%% begins in a good state also ends in a good state. This formula assumes
%% that the value of $\g$ at the beginning of $\foo$ is $\g$ before the
%% method call is $\gbef$, after the method call is $\gaft$ and at the
%% end of foo is $\gout$. Then if $(\g, \F) \satisfies \inv$ and $(\gaft,
%% \F) \satisfies \inv$ then $(\gbef, \F) \satisfies \inv$ and $(\gout,
%% \F) \satisfies \inv$.

%% The result from Observational purity checkers is only guaranteed to be
%% correct if the invariant $\inv$ is inductive. 

%% \begin{figure}[htp]
%%   \begin{algorithm}[H]
%%     \begin{align*}
%%       \mi{Is\mhyphen inductive} &\mi{(\inv : invariant, \pathCondition :
%%         path \; condition)} \equiv \\
%%       &(\forall \g) (\forall \gout)\\
%%       &(\forall \gaft_1) (\forall \gaft_2) \cdots\\
%%       &(\forall \gbef_1) (\forall \gbef_2) \cdots \{\\
%%       &\inv \wedge \inv[\gaft_1/\g] \wedge \inv[\gaft_2/\g] \cdots \\
%%       &\wedge \pathCondition \implies\\
%%       &\inv[\gout/\g] \wedge \inv[\gbef_1/\g] \wedge \inv[\gbef_2/\g] \cdots\\
%%       & \} \\
%%     \end{align*}
%%     \caption{Is-inductive : is true when $\inv$ is an inductive
%%       invariant} 
%%     \label{algo:inductivityCheck}
%%   \end{algorithm}  
%% \end{figure}

%% \section{Combining both checks}
%% Combining both the checks makes the whole approach more precise.

%% \subsection{Existential}

%% The formula presented in Algorithm~\ref{algo:someOPcheckCombined}
%% combines the existential check and the inductivity check. The
%% inductivity check in Algorithm~\ref{algo:inductivityCheck} checks if
%% invariant $\inv$ is inductive for all functions. Here we combine the
%% two checks and this will allow an invariant $\inv$ which is not
%% inductive for all functions, but only for the satisfying function
%% $\F$. 

%% This allows
%% a more precise result as the inductivity check here is a SAT query
%% instead of a VALID query and the invariant need not be inductive for
%% all functions.


%% \subsection{Impurity Witness}

%% \begin{figure}[htp]
%%   \begin{algorithm}[H]
%%     \begin{align*}
%%       \OPCheckA &\mi{(\inv : invariant,
%%         \pathCondition: path\; condition)} \equiv \\
%%       &\inv[\g_1/\g] \wedge \inv[\g_2/\g]\\
%%       & \wedge \inv[\gaft_{p1}/\g] \wedge \inv[\gaft_{p2}/\g] \cdots\\
%%       & \wedge \inv[\gaft_{q1}/\g] \wedge \inv[\gaft_{q2}/\g] \cdots\\
%%       &\wedge \n_1 = \n_2 \\
%%       &\wedge \pathCondition[\g_1/\g, \n_1/\n, \retVar_1/\retVar,
%%         \gout_1/\gout, \gbef_{p1}/\gbef_1 \cdots , \gaft_{p1}/\gaft_1\cdots]\\
%%       &\wedge \pathCondition[\g_2/\g, \n_2/\n, \retVar_2/\retVar,
%%         \gout_2/\gout, \gbef_{q1}/\gbef_1 \cdots , \gaft_{q1}/\gaft_1\cdots]\\
%%       &\wedge (\retVar_1 \neq \retVar_2 \vee \neg\inv[\gbef_{p1}/\g] \vee
%%       \neg\inv[\gbef_{p2}/\g] \cdots \vee \neg\inv[\gout/\g])\\
%%     \end{align*}
%%     \caption{Impurity witness (combined): produces a formula whose unsatisfiability
%%     implies weak observational purity}
%%     \label{algo:pairwiseOPcheckCombined}
%%   \end{algorithm}  
%% \end{figure}


\section{Generate invariant}\label{sec:growInvariant}

Algorithm `getInvariant' in Algorithm~\ref{algo:growInv} produces an
invariant given a set of global states. This procedure assumes that
the passed set of global states is a part of the invariant, lets call
this set of states $I_0$. Next, it assumes that procedure `\foo'
begins in a state in $I_0$ and finds the set of global states that are
feasible before the procedure call in `\foo' as well as the end of
`\foo', lets call these states 'new states'. Then $I_1 = I_0 +
\text{new sates}$. This is repeated to find $I_2$, where the new
states are the states discovered by assuming `\foo' begins in some
state in $I_1$. This repeats until a fix point is reached, i.e. until
$I_n = I_{n+1}$.

Algorithm~\ref{algo:growInv} in line no. 7 finds the values of global
variable $\g$ that may be feasible at the end of `\foo' assuming that
procedure `\foo' begins in a state that satisfies invariant $\inv$,
and adds it to $\inv'$. Next, in line 8 it adds value of $\g$ before
the first procedure call in `\foo' to $\inv'$. Then, adds the value of
$\g$ before the second procedure call in line 9 to $\inv'$, and so on
up to line 12. Then, it checks if a fix point has been reached (see
line no. 13), if yes then it sets $\mi{cond}$ to false. Otherwise, it
repeats.

Algorithm~\ref{algo:growInv} is a semi-decision procedure, It might
not terminate in some cases. This procedure assumes procedure `\foo'
to be observationally pure.  The invariant generated by this procedure
is a syntactic-invariant, because of the underlying assumption that
`\foo' is observationally pure. This invariant is also
semantic-invariant if `\foo' is observationally pure.

\begin{algorithm}[htp]
  \begin{algorithmic}[1]
      \STATE Formula getInvariant( Formula $\mi{initialInv}$, PathCondition
      $\pathCondition$) \{
      \STATE $\inv = \mi{initialInv}$;
      \STATE $\mi{cond = true}$;
      \STATE $free\mhyphen vars = \{\g, \gbef_1, \gbef_2
      \cdots \gbef_m,\gaft_1, \gaft_2 \cdots \gbef_m, \gout\}$;
      \WHILE{$\mi{cond}$}
      \STATE $k =  \inv \wedge \inv[\gaft_1]\cdots \wedge \inv[\gaft_m] 
      \wedge \pathCondition$;
      \STATE $\inv' = \inv \vee \forall(free \mhyphen vars - \{\gout\}).\,k[\g/\gout] \vee$
      \STATE $\forall(free \mhyphen vars - \{\gbef_1\}).\,k[\g/\gbef_1] \vee$
      \STATE $\forall(free \mhyphen vars - \{\gbef_2\}).\,k[\g/\gbef_2] \vee$
      \STATE $\forall(free \mhyphen vars - \{\gbef_1\}).\,k[\g/\gbef_1] \vee$
      \STATE $\cdots$
      \STATE $\forall(free \mhyphen vars - \{\gbef_m\}).\,k[\g/\gbef_m]$;
      \STATE $cond = (\inv' \implies \inv ? \mi{false:true}$);
      \STATE $\inv = \inv'$;
      \ENDWHILE
      \RETURN $\inv$;
  \end{algorithmic}
  \caption{ getInvariant($\formula$, $\pathCondition$) returns the
    invariant with seed global states as $\formula$, if it terminates}
  \label{algo:growInv}
\end{algorithm}


\end{document}
