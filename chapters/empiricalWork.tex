\documentclass[../thesis.tex]{subfiles}
\begin{document}
\chapter{Empirical Work}\label{cha:empirical}
We discuss the implementation issues and examples that illustrate our
approach.  
\section{Details of our Implementation}
We have built our analysis on top of Boogie
framework~\cite{leino2008boogie}. Boogie is a framework for
verification and also a programming language. 

Using Boogie we convert a program in boogie programming language to
formulae in first order logic. Given a loop free procedure, we first
represent it as a set of paths. For example procedure `factorial' in
Listing~\ref{lst:factBpl} has four paths, presented in
Figure~\ref{fig:pathFormulaeFactorial}. Next, we use boogie framework
to convert each path to a formula in first order logic. For instance
Figure~\ref{fig:pathFormulaeFactorial} gives the formulae for paths of
procedure `factorial'.

Now given the invariant and the formulae for paths of a procedure, we
have implemented two methods that generate formulae
$\formula_{existential}$ and $\formula_{impurity}$. These two formulae
$\formula_{existential}$ and $\formula_{impurity}$ are equivalent to
formulae produced by Algorithm~\ref{algo:someOPcheckCombined} and
Algorithm~\ref{algo:pairwiseOPcheckCombined} respectively.

%% We use their work to convert segments of code into a formulae in first
%% order logic which preserves the behaviours of the code. Next we add
%% constraints to generate the path condition. Using the path condition
%% it was straight forward to implement the observational purity
%% checkers.

Next we check formulae $\formula_{existential}$ and
$\formula_{impurity}$ using Z3~\cite{de2008z3}. If $\formula_{existential}$
is satisfiable, it implies that the given procedure is observationally
pure. Also if formula $\formula_{impurity}$ is unsatisfiable, it
implies that the given procedure is observationally pure.

Other than this, we have also implemented an iterative procedure to
generate the invariant. This procedure takes a set of seed states, and
grows the set of reachable global states until a fix-point is
reached. This procedure effectively emulates
Algorithm~\ref{algo:growInv}. 

\section{Benchmark Examples}
The programs that we consider for evaluation are written by the
author and there is no established set of benchmark programs for this
problem.

\lstinputlisting[label=lst:factBpl, caption={Recursive
    version of factorial in Boogie pl}]{examples/fact1.bpl}

Procedure `factorial' in Listing~\ref{lst:factBpl} for an argument
value n returns `n!'. Procedure `factorial' in
Listing~\ref{lst:factBpl} is the analogue of procedure `factorial' in
Listing~\ref{lst:factorial}, in boogie programming language.
Procedure `factorial' caches the result for argument
value `19'. The invariant on the global states we used for this
example was also automatically generated, it is `$g = -1 \vee g =
\F(18)*19$'. The above mentioned invariant was generated that is
effectively equivalent to Algorithm~\ref{algo:growInv}.

For the above mentioned example of procedure `factorial' the invariant
`$g = -1 \vee g = \F(19)$' is also valid but it is not suitable for
the impurity witness based approach. The reason is that the impurity
witness approach cannot reason that $\F(18) * 19 = \F(19)$. The
impurity witness approach, while comparing path (b) and (c) in
Figure~\ref{fig:pathFormulaeFactorial} would get the sub formula
$\F(18) * 19 = \retVar_b \wedge \retVar_c = g_c \wedge g_c = \F(19)
\wedge \retVar_b \neq \retVar_c$. And this formula can be simplified
to $\F(18) * 19 \neq \F(19)$. Now there are many interpretations that
would satisfy $\F(18) * 19 \neq \F(19)$. Whereas if we use the
invariant $g = -1 \vee g =\F(18) * 19$, we get the constraint $\F(18)
* 19 \neq \F(18) * 19$. And there are no interpretations that would
not satisfy $\F(18) * 19 \neq \F(18) * 19$.

On the contrary, if we use the invariant `$g = -1 \vee g = \F(19)$'
for procedure `factorial' using the existential approach, there is no
problem. Al thought, in this approach we get the constraint $\F(18) *
19 = \F(19)$, it does not cause the formula to be unsatisfiable. This
is one benefit of the existential approach.

\lstinputlisting[caption=Recursive version of Fibonacci in Boogie
    pl., label=lst:fibBpl]{examples/fib.bpl}

Procedure `fib' in Listing~\ref{lst:fibBpl} computes the n$^{th}$
Fibonacci number for argument value `n'. It caches each result for
later use. The invariant used here encodes that at each index, `i' of
the array `cache', the value stored is either `0' or `$\F(i - 1) +
\F(i - 2)$'. The size of array `cache' is unbounded. The only way to
represent the values of an unbounded array is using quantifiers.
Thus, it is out of scope of Algorithm~\ref{algo:growInv}. For this
procedure the invariant we provided manually is $\forall i. cache[i] =
\F(i-1) + \F(i -2) \vee cache[i]= 0$.

For the same reason as above (in the case of the procedure
`factorial') we cannot use the invariant $\forall i. cache[i] = \F(i)
\vee cache[i]= 0$ for procedure `fib' in Listing~\ref{lst:fibBpl}
while using the impurity witness approach. Whereas the existential
approach has no problem with the same invariant. Here the impurity
witness approach is not able to conclude that $\forall i. \F(i) = \F(i
- 1) + \F(i -2)$. Similar to the reasoning above the impurity witness
approach produces the sub formula $(\forall i. cache[i] = \F(i) \vee
cache[i] = 0) \wedge \retVar_{path2} = cache[n_{path2}] \wedge
\retVar_{path3} = \F(n_{path3} - 1) + \F(n_{path3} - 2) \wedge
\retVar_{path2} \neq \retVar_{path3}$. And this formula is
satisfiable. 

Procedures `mcm' and `foo' in Listing~\ref{lst:mcmBpl} is the most
interesting example of an observationally pure method in this
thesis. It implements the recursive variant of matrix chain
multiplication. Thus `mcm(i,j)' returns the minimum number of multiplications
needed to multiply j - i -1 matrices with dimensions of the matrices
stored in array p. Procedure `foo' in Listing~\ref{lst:mcmBpl} finds the
split with the least cost, which is done using a loop in the standard
implementation of matrix chain multiplication. As discussed in
Section~\ref{sec:multiProc} a system of mutually recursive procedures
could also be proved observationally pure using our technique. But in
this example procedure `foo' is trivially observationally pure if
procedure mcm is observationally pure as it does not read any varying
global state or to any global state. Thus only procedure `mcm' needs
to be checked for observational purity. The invariant on the global
states essentially encoded $\forall i \forall j. m[i,j] = F(i,i, j ,
-1) \vee m[i,j] = 0$.

\lstinputlisting[caption={Recursive version of matrix chain
      multiplication in Boogie pl}, label=lst:mcmBpl]{examples/mcm1.bpl}

\section{Results}\label{sec:results}

In this section we presented how did both of our approaches perform on
examples.
For all the programs mentioned above the SMT solver timed out in the
case of the existential approach. Whereas the impurity witness
approach successfully marked all the examples as observationally pure.
\end{document}
