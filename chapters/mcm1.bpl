var p: [int] int;
var m: [int, int] int;
procedure foo(k: int, i: int, j:int, min :int) returns (r: int) modifies m;{
	var a, b, q : int;
	var min1 :int;
	if(k >= j) {
		r:= 0;
	} else {
		call a := mcm(i, k);
		call b := mcm(k+1, j);
		q := a + b + p[i-1] * p[k] * p[j];
		if( min < 0) {
			min1 := q;
		}
		if( q < min) {
		  min1 := q;
		}		
		call r := foo(k + 1, i, j, min1);
	}	
}

procedure {:entrypoint} mcm(i: int, j: int) returns (r: int) modifies m;{
	var k, q : int;
	var a, b :int;
	if(i == j) {
		m[i, j] := 0;
		r := 0;
	} else {
		if( m[i, j] > 0) {
			r := m[i, j];
		} else {
			k := i;
			call r := foo(k, i, j, m[i, j]);
			m[i, j] := r;
		}
	}
}