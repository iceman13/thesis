\documentclass[../thesis.tex]{subfiles}
\begin{document}
\chapter{Proof of Correctness}\label{cha:proof}

In this chapter we will give the proof of correctness of the
existential and the impurity witness approaches for recursive
programs. We prove that if the two approaches
(Algorithm~\ref{algo:someOPcheckCombined} and
Algorithm~\ref{algo:pairwiseOPcheckCombined}) mark procedure `\foo' as
observationally pure then for all executions of `\foo' that begin in a
global state $\gstate$ such that $(\gstate, \F) \satisfies \inv$
respect function $\F$. Also, we prove that each trace that begins in a
global states such as $\gstate$ ends in a global state $\gstate'$ such
that $(\gstate', \F) \satisfies \inv)$. Next, we define the some
terminology and then present the proofs.

\begin{definition}[segment(trace, $\integer$)
    $\rightarrow$ trace segment]
  $\segment{\trace}{n}$ returns the $n^{th}$ trace-segment of trace
  $\trace$. The trace-segments of a trace are ordered in the order of
  execution.
\end{definition}

\begin{definition}[segment-depth(trace segment, trace)$\rightarrow
    \integer$)] Let $\trace$ be trace and $\tSegment$ be a trace
  segment in $\trace$. Let $\tSegment' = \segment{\trace}{1}$.
  $\segDepth{\tSegment}{\trace}$ is the difference in depth of stack
  in $\tSegment$ and $\tSegment'$, plus 1.
\end{definition}

\begin{definition}[trace-depth(trace $\rightarrow \integer$)]
  $\traceDepth{\trace}$ is defined as the max of
  \\$\segDepth{\tSegment}{\trace}$, such that $\tSegment$ ranges over
  the trace segments of trace $\trace$.
\end{definition}

%% \begin{definition}[valid invariant($\inv$ : Formula, $\F$ :
%%     Function, `\foo' : procedure) ]\label{def:validInvariant}
%%   $\\\vi{\inv}{\F}$ is a boolean property that is true if for all
%%   traces $\trace$ of procedure `\foo',
%%   \begin{enumerate}
%%   \item $(\atEntry{\trace}, \F) \satisfies \inv$.
%%   \item if $\trace$ has a procedure call, then $(\beforeCall{\trace},
%%     \F ) \satisfies \inv$.
%%   \item if $\trace$ has a procedure call statement, say $x = \foo(y)$
%%     and if we replace it with $x = F(y)$ and change the value of the
%%     global variable $\g$ to a value $\g'$ such that $(\g', \F)
%%     \satisfies \inv$ then %% $\trace$ respects $\F$ and
%%     $(\atEnd{\trace}, \F) \satisfies \inv$.
%%   \item $(\atEnd{\trace}, \F) \satisfies \inv$.
%%   \end{enumerate}  
%% \end{definition}
 
%% \begin{definition}[consistent interpretation( Function :
%%     $\F$, Formula : $\inv$)] \label{def:consistentInterpretation}
%%   $\\\ci{\F}{\inv}$ is a boolean property that is true if for all
%%   traces $\trace$ of a procedure `\foo', $(\atEntry{\trace}, \F)
%%   \satisfies \inv$, and if $\trace$ has a procedure call, say $x =
%%   \foo(y)$ and if we replace it with $x = \F(y)$ and change the the
%%   value of the global variable $\g$ to $\g'$ such that $(\g',\F)
%%   \satisfies \inv$ then $\trace$ respects function $\F$.
%% \end{definition}

\begin{definition}[valid and consistent invariant( Function,
    Formula)]
  \label{def:validConsistentInvariant}
  $\\\vci{\F}{\inv}$ is a boolean property that is true if for all
  traces and prefix-trace-segments $x$ of procedure `\foo' such that
  $(\atEntry{x}, \F) \satisfies \inv$ the following holds

  \begin{enumerate}
  \item if $x$ is a trace.
    \begin{enumerate}
    \item If $x$ has a recursive call
      \begin{enumerate}
      \item then $(\beforeCall{x}, \F) \satisfies \inv$ and
      \item let $x$ have the procedure call statement, say $y =
        \foo(z)$. Then we change the value of $y$ to $y'$ and the
        value of the global variable $\g$ to $g'$ after the procedure
        call statement such that $y' = \F(z)$ and $(g', \F)
        \satisfies \inv$.
      \end{enumerate}
      Then $\return{x} = \F(\n)$, where $\n$ is the argument to
      trace $x$ and $(\atEnd{x}, \F) \satisfies \inv$.
    \item if $x$ does not have a recursive call
      then $(\atEnd{x}, \F) \satisfies \inv$ and $\return{x} =
      \F(\n)$, where $\n$ is the argument to trace $x$.
    \end{enumerate}
  \item if $x$ is a prefix trace segment.\\
    Then $(\atEnd{x}, \F) \satisfies \inv$.
  \end{enumerate}
\end{definition}

\begin{definition}[static observational purity(invariant) ]
  Procedure `\foo' is static observationally pure wrt a given
  invariant $\inv$ if there
  exists a function $\F$ such that\\ $\vci{\F}{\inv}$.
\end{definition}

%% In this Chapter we will prove that if the main procedure,
%% algorithm~\ref{algo:main} returns true, then procedure $\foo$ mimics a
%% mathematical function. First we prove two properties about the tuple
%% $\{\inv$, $\F$, $\foo\}$ namely $\vi{\inv}{\F}$ and\\ $\ci{\F}{\inv}$,
%% which serve as a contract with the implementation. Then using these
%% two properties we inductively prove that a set of traces of $\foo$
%% agree with a function that satisfies formula $\inv$. The set of traces
%% considered are the traces that begin in a good state. A state $\state$
%% is a \emph{good state} if $(\state, \F) \satisfies \inv$. We show that
%% a trace that begins in a good state also ends in a good state and
%% respects interpretation $\F$. Hence all histories beginning in a good
%% state agree with a mathematical function.
\section{Proof of correctness of Existential check}
\subsection{Overview of the proof}
First we prove a property about the given tuple $\{\inv$, $\F$, $\foo\}$
namely\\ $\vci{\F}{\inv}$, which serves as a contract with the
implementation. Then using this property we inductively prove that 
for each trace $\trace$, $(\atEntry{\trace}, \F) \satisfies \inv
\implies (\atEnd{\trace}, \F) \satisfies \inv$ and $\trace$ respects
function $\F$.
\subsection{Proof}


\begin{lemma}
  If $\OPCheckE\mi{(\inv : invariant, \pathCondition : path \;
    condition)}$ (Algorithm~\ref{algo:someOPcheckCombined}) returns
  formula $\formula$ and $\F'$ be any function such that $\F'
  \satisfies \formula$ then\\ $\vci{\inv}{\F'}$ holds.
\end{lemma}

\begin{proof}
  In Algorithm~\ref{algo:someOPcheckCombined} the returned formula is
  $\formula$ and $\F' \satisfies \formula$. Lets substitute $\F'$ in
  $\formula$ for the free variable $\F$ to get $(\forall \g) (\forall
  \n) (\forall \retVar) (\forall \gout) (\forall \gbef_1) (\forall
  \gbef_2) \cdots (\forall \gaft_1) (\forall \gaft_2) \cdots \{\inv[\F'/\F]
  \wedge \pathCondition[\F'/\F] \wedge \inv[\gaft_1/\g, \F'/\F] \wedge
  \inv[\gaft_2/\g, \F'/\F] \cdots \implies (\retVar = \F'(\n) \wedge
  \inv[\gout/\g, \F'/\F] \wedge \inv[\gbef_1/\g, \F'/\F] \wedge
  \inv[\gbef_2/\g, \F'/\F]
  \cdots) \}$, lets call this formula $\formula'$.

  $\formula'$ is valid, since $\F' \satisfies \formula$ and $\formula'
  = \formula[\F'/\F]$. Let $x$ be a trace or a trace segment, such
  that $(\atEntry{x}, \F') \satisfies \inv$ then
  \begin{enumerate}
  \item if $x$ is a trace.
    \begin{enumerate}
    \item if $x$ has a recursive call
      \begin{enumerate}
      \item \label{beforeCallInRec} $(\beforeCall{x}, \F') \satisfies
        \inv$ holds as the conjuncts in the conclusion of $\formula'$,
        \\$\inv[\gbef_1/\g, \F'/\F]$, $\inv[\gbef_2/\g, \F'/\F] \cdots
        \inv[\gbef_n/\g, \F'/\F]$ are satisfied for each $i^{th}$ call
        statement.
      \item the premise in $\formula'$, the conjunct
        $\pathCondition[\F'/\F]$, constraints the recursive call's
        return value to respect function $\F'$. Next, the 
        conclusion in formula $\formula'$ has the conjuncts
        $\inv[\gout/\g,  \F'/\F]$ and $\retVar = \F'(\param{x})$. Thus,
        $(\atEnd{x}, \F') \satisfies \inv$ and $\return{x} =
        \F'(\param{x})$.
      \end{enumerate}
    \item if $x$ does not have a recursive call.\\ In the conclusion
      in formula $\formula'$, we have $\inv[\gout/\g, \F'/\F]$ and\\
      $\retVar = \F'(\param{x})$. Thus, we have $(\atEnd{x}, \F')
      \satisfies \inv$ and $\return{x} = \F'(\param{x})$.
    \end{enumerate}
  \item If $x$ is a prefix trace segment \\we have, 
    trace $\trace$ must exist such that $\segDepth{x}{\trace} =
    1$. And we have\\ $(\atEntry{x}, \F') \satisfies \inv$. Now we can
    apply the above mentioned point \ref{beforeCallInRec}, we get
    $(\beforeCall{\trace}, \F') \satisfies \inv$, and
    $\beforeCall{\trace} = \atEnd{x}$. Thus, we have $(\atEnd{x}, \F')
    \satisfies \inv$.
  \end{enumerate}
  
  Hence proved.
\end{proof}

\begin{lemma} \label{lemma:trace}
Let $\trace$ be any trace, $\inv$ be any formula and $\F$ be any
mathematical function such that $\vci{\F}{\inv}$ and
$(\atEntry{\trace}, \F) \satisfies \inv$ then,\\ $\return{\trace} =
\F(\param{\trace})$ and $(\atEnd{\trace}, \F) \satisfies \inv$.
\end{lemma}

\begin{proof}
  To prove this lemma we will first show that for each transitive sub-trace
  $\subTrace$ of $\trace$, \\$(\atEntry{\subTrace}, \F) \satisfies
  \inv$. Next, Trace $\trace$ will have an innermost trace (say
  $\subTrace^0$), which by definition cannot have a recursive
  call. Sub-trace $\subTrace^0$ respects function $\F$ because
  \\$\vci{\F}{\inv}$ holds. Next, we will prove that the sub-trace
  surrounding $\subTrace^0$, say $\subTrace^1$ respects function $\F$
  as $\vci{\F}{\inv}$ holds. Next, the sub-trace surrounding
  $\subTrace^1$. And so on. This way, one by one, starting from the
  inner most sub-trace we prove that each surrounding sub-trace
  respects function $\F$.

  Inductive Hypothesis $P(n)$: Let $\inv$ be a formula, $\F$ be an
  function, $\trace$ and $\subTrace$ be any traces, $\tSegment$ be a
  trace-segment such that $\vci{\F}{\inv}$, $(\atEntry{\trace}, \F)
  \satisfies \inv$, $\tSegment = \segment{\trace}{n}$,
  $(\atEntry{\tSegment}, \F) \satisfies \inv$,
  $\segDepth{\tSegment}{\subTrace} = 1$, then $(\atEnd{\tSegment}, \F)
  \satisfies \inv$ and if $\tSegment$ is a suffix-trace-segment or a
  whole-trace-segment then $\return{\subTrace} =
  \F(\param{\trace})$.
  
  Base case : Let $\tSegment = \segment{\trace}{0}$. We have,
  $(\atEntry{\trace}, \F) \satisfies \inv$, thus $(\atEntry{\tSegment},
  \F) \satisfies \inv$ as $\atEntry{\tSegment} =
  \atEntry{\trace}$. $(\atEnd{\tSegment}, \F) \satisfies \inv$, as
  $\vci{\F}{\inv}$ holds. If $\tSegment$ has a return statement then,
  $\tSegment$ is a whole-trace-segment, $\subTrace = \tSegment =
  \trace$ and $\return{\subTrace} = \F(\n)$ holds because of
  $\vci{\F}{\inv}$.

  Inductive case: Assuming $P(0), P(1) \cdots P(n)$ and
  proving $P(n+1)$. Let $\tSegment =
  \segment{\trace}{n+1}$. $\tSegment_{pre} = \segment{\trace}{n}$. Let
  $\trace_{pre}$ be a trace such that
  $\segDepth{\tSegment_{pre}}{\trace_{pre}} = 1$.
  $\atEntry{\tSegment} = \atEnd{\tSegment_{pre}}$ ,thus
  $(\atEntry{\tSegment}, \F) \satisfies \inv$. Now, we have 3 cases
  depending upon whether $\tSegment$ is a prefix, suffix or a whole
  trace segment.

  \begin{enumerate}
  \item If $\tSegment$ is prefix-trace-segment.\\
    We have $\tSegment_{pre}$ is a prefix-trace-segment.  Next,
    $(\atEnd{\tSegment},\F) \satisfies \inv$ as \\$\vci{\F}{\inv}$
    holds.

  \item If $\tSegment$ is a whole-trace-segment.\\
    We have $\trace_{pre}$ is a prefix-trace-segment. $\tSegment =
    \subTrace$ as $\tSegment$ is a whole-trace-segment and from
    $\vci{\F}{\inv}$ we have $(\atEnd{\subTrace}, \F) \satisfies \inv$
    and $\return{\subTrace} = \F(\n)$.

  \item If $\tSegment$ is suffix-trace-segment.\\
    $\tSegment_{pre}$ is either a suffix-trace-segment or a
    whole-trace-segment, and from the
    inductive hypothesis we have $\return{\trace_{pre}} = \F(\n)$. let
    $\tSegment_{other}$ be the other trace-segment in trace
    $\subTrace$, $\tSegment_{other}$ is a
    prefix-trace-segment. $\tSegment_{other} = \segment{\trace}{y}$
    and $y < n$, because of the structure of trace $\subTrace$. Trace
    $\subTrace$ is equivalent to trace-segment $\tSegment_{other}$ followed by a
    recursive call, then followed by trace-segment $\tSegment$.  Also
    $\atEntry{\subTrace} = \atEntry{\tSegment_{other}}$, thus
    $(\atEntry{\subTrace}, \F)
    \satisfies \inv$ from the inductive hypothesis. Also
    $\beforeCall{\subTrace} = \atEnd{\tSegment_{other}}$, and from the
    inductive hypothesis we have $(\atEnd{\tSegment_{other}}, \F)
    \satisfies \inv$, thus $(\beforeCall{\subTrace}, \F)
    \satisfies \inv$. Now, as $\vci{\F}{\inv}$ holds we
    have, $(\atEnd{\subTrace}, \F) \satisfies \inv$ and
    $\return{\subTrace} = \F(\n)$.
  \end{enumerate}
  
  Hence proved.
\end{proof}

\begin{theorem} \label{theorem:swopGivesWop}
  Let $\inv$ be a formula and $\F$ be a function such that
  \\$\OPCheckE\mi{(\inv : invariant, \pathCondition : path \;
    condition)}$ returns formula $\formula$ and $\F \satisfies
  \formula$. Then procedure `\foo' is observationally pure wrt all
  global states $\gstate$ such that $(\gstate, \F) \satisfies \inv$.
\end{theorem}

\begin{proof}
  Inductive hypothesis $P(k)$ : Let $\inv$ be an invariant, $\history$
  be a history, $\gstate$ be a global state, such that $(\atEntry{\history},
  \F) \satisfies \inv$, $(\gstate, \F)
  \satisfies \inv$, the $k^{th}$ trace in $\history$ be $\trace_k$
  then $(\atEntry{\trace_k}, \F) \satisfies \inv$, $(\atEnd{\trace_k},
  \F) \satisfies \inv$ and $\return{\trace_k} = \F(\n_k)$ where $\n_k$
  is the argument to trace $\trace_k$.
  
  Base case : Let trace $\trace_1$ be the first trace of history
  $\history$ and since $\atEntry{\history} = \atEntry{\trace_1}$ we
  have $(\atEntry{\trace_1}, \F) \satisfies \inv$. From
  lemma~\ref{lemma:trace} we have $(\atEnd{\trace_1}, \F) \satisfies
  \inv$, and also $\return{\trace_1} = \F(\n_1)$.
  
  Inductive case : Assuming P(k) and proving P(k + 1). From the
  inductive hypothesis we have $(\atEnd{\trace_k}, \F) \satisfies
  \inv$. Also, $\atEnd{\trace_k} = \atEntry{\trace_{k+1}}$. Thus,
  $(\atEntry{\trace_{k+1}}, \F) \satisfies \inv$ and from
  lemma~\ref{lemma:trace}, we can conclude $(\atEnd{\trace_{k+1}}, \F)
  \satisfies \inv$ and $\return{\trace_{k+1}} = \F(\n_{k+1})$ where
  $\n_{k+1}$ is the argument to $\trace_{k+1}$.

  Hence proved.
\end{proof}

\section{Proof of correctness for Impurity witness}
In this section we will prove that if Impurity witness procedure
(Algorithm~\ref{algo:pairwiseOPcheckCombined}) returns formula
$\formula$ for procedure `\foo' and $\formula$ is UNSAT then procedure
`\foo' is observationally pure.

The formula produced by $\OPCheckA \mi{(\inv, \pathCondition)}$
for \label{opcheckBreakdown} simplicity can be broken down into two
parts. Let the formula produced by $\OPCheckA \mi{(\inv,
  \pathCondition)}$ be $\pairFormula$ and $\pairFormula = \formula_1
\wedge \formula_2$. Where $\formula_1$ is : \\
$\inv[\g_1/\g] \wedge \inv[\g_2/\g] \wedge
\inv[\gaft_{p1}/\g] \wedge \inv[\gaft_{p2}/\g] \cdots \wedge
\inv[\gaft_{q1}/\g] \wedge \inv[\gaft_{q2}/\g] \cdots \wedge \n_1 =
\n_2 \wedge \pathCondition[\g_1/\g, \n_1/\n, \retVar_1/\retVar,
\gout_1/\gout, \gbef_{p1}/\gbef_1 \cdots,
\gaft_{p1}/\gaft_1\cdots] \wedge \\ \pathCondition[\g_2/\g,
\n_2/\n, \retVar_2/\retVar, \gout_2/\gout, \gbef_{q1}/\gbef_1
\cdots , \gaft_{q1}/\gaft_1\cdots]$\\
And $\formula_2$ is :\\
$(\retVar_1 \neq \retVar_2 \vee \neg\inv[\gbef_{p1}/\g] \vee
\neg\inv[\gbef_{p2}/\g] \cdots \vee \neg\inv[\gout/\g])$

\begin{lemma}\label{lemma:funcRef}
  $\forall \F \exists \F_{outer} \forall (X) \formula_1 \implies
  \retVar_1 = \F_{outer}(n_1)$, $\forall \F \forall(X) \formula_1
  \implies \retVar_1 = \retVar_2$ and $\forall \F \exists \F_{outer}
  \forall (X) \formula_1 \implies \retVar_1 = \F_{outer}(n_1) =
  \retVar_2$ be formulae $\formula_{func}$, $\formula_{ret}$ and $\formula_3$
  respectively then $\formula_{func} = \formula_{ret} = \formula_3$.
\end{lemma}
\begin{proof}
  We will prove that both $\formula_{func}$ and $\formula_{ret}$ are
  equivalent to formula $\formula_3$.
  \begin{enumerate}
  \item Proving $\formula_{func}$ and $\formula_3$ equivalent.\\ In
    formula $\formula_1$ there are two instances of
    $\pathCondition$. For one instance of $\pathCondition$ variable
    $\retVar$ is rename
    to $\retVar_1$ whereas for the other, it is renamed to
    $\retVar_2$. And all variables have equivalent
    constraints.
    Thus, if there exist 
    an assignment for one instance of subformula
    $\pathCondition$ in $\formula_1$, the same assignment can be
    mapped to the other instance of $\pathCondition$.
    %% is also and assignment for the
    %% other instance
    Hence $\retVar_1 = \retVar_2$.\\
    Thus, we have $\formula_{func}$ and $\formula_3$ are equivalent.
  \item Proving $\formula_{ret}$ and $\formula_3$ equivalent.\\
    Here we will show that given $\formula_{ret}$ is valid, we can
    construct a function $\F$ such that for each domain value $n_1$,
    it would map it function $\F$ to value $\retVar_1$.
    
    Proving by contradiction. Let us assume that $\formula_{ret}$
    holds, but we cannot construct a
    function $\F$ such that it would be equivalent to $\retVar_1$ for
    each domain value. Let us consider the case that a value
    from the domain is not mapped to a unique value in the range.
    Since function $\F$ maps 
    %% (see section \ref{onlyTerminatinTraces}). Since function $\F$ maps
    variable $\n_1$ to $\retVar_1$ and such a function cannot exist;
    it means that $\retVar_1 \neq \retVar_2$ is possible, which
    implies $\formula_{ret}$ is not valid.
    %% The other
    %% case is that a value from the domain is not mapped to an element
    %% in the range. Such a case would arise if the trace is non
    %% terminating. And in this case both the formulae are true.
    Hence this
    is a contradiction.

    Hence proved.
  \end{enumerate}
\end{proof}

\begin{lemma}\label{lemma:forAllOuterAInner}
  Let $\forall \F \exists \F_{outer} \forall (X) \formula_1
  \implies (\neg \formula_2 \wedge \retVar_1 = \F_{outer}(\n_1))$ and
  $\forall \F \forall (X) \formula_1 \implies \neg \formula_2$ be
  $\formula_3$ and $\formula_4$ respectively. $\formula_3$ and
  $\formula_4$ are equivalent.
\end{lemma}

\begin{proof}
\begin{align*}
  \formula_3 =&\\
  &\forall \F \exists \F_{outer} \forall (X) \formula_1
  \implies (\neg \formula_2 \wedge \retVar_1 = \F_{outer}(\n_1))\\
=&\forall \F \exists \F_{outer} \forall (X) \formula_1 \implies
(\neg(\retVar_1 \neq \retVar_2 \\
&\vee \neg\inv[\gbef_{p1}/\g] \vee \neg\inv[\gbef_{p2}/\g] \cdots 
\vee \neg\inv[\gout/\g]) \wedge\\
&\retVar_1 = \F_{outer}(\n_1))\\
=&\forall \F \exists \F_{outer} \forall (X) \formula_1 \implies
(\retVar_1 = \retVar_2 = \F_{outer}(\n_1) \wedge\\
&\inv[\gbef_{p1}/\g] \wedge \inv[\gbef_{p2}/\g] \cdots \wedge \inv[\gout/\g])\\
=&\forall \F \exists \F_{outer} \forall (X) \formula_1 \implies
(\retVar_1 = \retVar_2 = \F_{outer}(\n_1)) \wedge\\
&\forall \F \exists \F_{outer} \forall (X) \formula_1 \implies
\inv[\gbef_{p1}/\g] \wedge
\inv[\gbef_{p2}/\g]\\
&\cdots \wedge \inv[\gout/\g]\\
=&\forall \F \forall (X) \formula_1 \implies
\retVar_1 = \retVar_2 \wedge  &(lemma~\ref{lemma:funcRef})\\
&  \forall \F \exists \F_{outer} \forall (X) \formula_1 \implies (
\inv[\gbef_{p1}/\g] \wedge \\
&\inv[\gbef_{p2}/\g] \cdots \wedge \inv[\gout/\g])\\
=&\forall \F \forall (X) \formula_1 \implies
\retVar_1 = \retVar_2 \wedge &(\F_{outer} \not \in free\mhyphen
var(a) a \in \{\formula_1, \inv\})\\
&\forall \F \forall (X) \formula_1 \implies (
\inv[\gbef_{p1}/\g] \wedge \inv[\gbef_{p2}/\g] \cdots \wedge\\
&\inv[\gout/\g])\\
=&\forall \F \forall (X) \formula_1 \implies (
\retVar_1 = \retVar_2 \wedge \inv[\gbef_{p1}/\g] \wedge \\
&\inv[\gbef_{p2}/\g] \cdots \wedge \inv[\gout/\g])\\
=&\forall \F \forall (X) \formula_1 \implies \neg
\formula_2\\
=& \formula_4\\
%% =&\forall \F \forall (X) \formula_1 \implies
%% \retVar_1 = \retVar_2 \wedge
%% &(\F_{outer} \not \in free\mhyphen variable(\formula_1))\\
%% &\forall \F \exists \F_{outer} \forall (X) \formula_1 \implies (
%% \inv[\gbef_{p1}/\g] \wedge \inv[\gbef_{p2}/\g] \cdots \wedge
%% \inv[\gout/\g])\\
%% =&\forall \F \forall (X) \formula_1 \implies
%% \retVar_1 = \retVar_2 = \F(n_1) \wedge
%% & (lemma~\ref{lemma:funcRef})\\
%% &\forall \F \exists \F_{outer} \forall (X) \formula_1 \implies (
%% \inv[\gbef_{p1}/\g] \wedge \inv[\gbef_{p2}/\g] \cdots \wedge
%% \inv[\gout/\g])\\
%% =&\forall \F \exists \F_{outer} \forall (X) \formula_1 \implies (
%%  \retVar_1 = \retVar_2 = \F(n_1)\wedge\\
%% &\inv[\gbef_{p1}/\g] \wedge \inv[\gbef_{p2}/\g] \cdots \wedge
%%  \inv[\gout/\g])\\
%%  =&\forall \F \exists \F_{outer} \forall (X) \formula_1 \implies (
%%  \neg \formula_2 \wedge \retVar_1 = \F(n_1))\\
%%  =& \formula_3
\end{align*}
Hence proved.
\end{proof}

\begin{lemma}\label{lemma:combiningF}
  Let $\exists \F \forall (X) \formula_1 \implies (\neg \formula_2
  \wedge \retVar_1 = \F(\n_1))$ and $\forall \F \exists \F_{outer}
  \forall (X) \formula_1 \implies (\neg \formula_2 \wedge \retVar_1 =
  \F_{outer}(\n_1))$ be Formulae $\formula_{merged}$ and
  $\formula_{outer}$ respectively. Then $\formula_{outer} \implies
  \formula_{merged}$.
\end{lemma}

\begin{proof}
  Assuming $\formula_{outer}$ is valid we will inductively build a
  function $\F'$ such that we can substitute it for both functions
  $\F$ and $\F_{outer}$ in formula $\formula_{outer}$. Thus,
essentially finding a function that satisfies formula
$\formula_{merged}$.


  %% $(\forall (X) \formula_1 \implies (\neg
  %% \formula_2 \wedge \retVar_1 = \F(\n_1)))[\F'/\F]$ is valid.

  %% In order to build this function $\F'$ we need tools to traverse over
  %% the domain of $\F'$ with respect to the formula
  %% $\formula_{outer}$. The following are the definitions.
We will use the following definitions to traverse the domain of
function $\F'$ with respect to formula $\formula_{outer}$.

  \begin{definition}[depth(dom $\F \rightarrow \integer$)]
$\depth{n} = 1 + min(\depth{k})$ where $n \in dom \;\F'$ such that
$(\forall (X) \formula_1 \implies (\neg
  \formula_2 \wedge \retVar_1 =
\F_{outer}(\n_1)))[\F'(\n)/\F_{outer}(\n_1), \F'(k)/\F(k_1),
\F'(k)/\F(k_2)]$ is valid, where $k_1$ and $k_2$ are the arguments to
the inner procedure calls in the two instances of $\pathCondition$ in
formula $\formula_1$. Next, $x$ such that $x \in dom \; \F'$ has depth
0 if in the above mentioned formula by substituting $x$ for $\n$ leads
to a case such that value of $\retVar_1$ is independent of $k_1$.

    %% value $\F(k)$ is substituted in formula $\formula_{outer}$ to get
    %% the mapping for $n$. $\depth{n} = 0$ if no substitutions are
    %% necessary to get the mapped value for $n$.
  \end{definition}

  \begin{definition}[level($\integer$) $\rightarrow$ subset of dom $\F$]
    $\level{n}$ gives the set $X$ of domain elements of $\F$ such
    $\forall v \in X \depth{v} \leq n$.
\end{definition}

  Inductive hypothesis $P(n)$ : Let $\formula_{outer}$ be valid then
  there exist a function $\F'$ such that $\forall k \in \level{n}
  \implies \{\forall (X-\{n_1\}) \formula_1 \implies (\neg
  \formula_2 \wedge \retVar_1 = \F_{outer}(\n_1))\}[\F'/\F_{outer},
    k/n_1, \F'/\F]$.

Base case $P(0)$ : Let $p \in \level{0}$. The corresponding value for
$p$ can be produced only if $\pathCondition$ has a disjunct which does
not refer the function $\F$, otherwise $\level{0}$ is an empty set. If
$\level{0}$ is not an empty set then, it satisfies the inductive
hypothesis, as all satisfying functions replacing $\formula_{outer}$
respect each other on these arguments. This is because the mappings
are essentially encoded in the formula itself and $\formula_{outer}$
holds. If $\level{0} = \phi$ then the hypothesis holds vacuously.

%% $\level{0}$ can be empty only if procedure `\foo' does
%%   not have a base case thus, `\foo' never terminates; in this case we
%%   are done. If $\level{0}$ is not empty, since $\formula_{outer}$ is
%%   valid we have that for all functions substituted for the inner
%%   procedure call, there exists a function that respects the value
%%   $\retVar$; function $\F'$ also respects $\retVar$.
  
Inductive case : Assuming $P(k)$ holds, proving for $P(k+1)$. We have
that up-to the $k^{th}$ level there exists a function $\F'$ that
satisfies the inductive hypothesis. Next, we can find the values of
the $\n$ such that it leads to an element of the set $\level{k}$ as
the argument to the inner procedure call in procedure $\foo$. Thus,
essentially giving us the elements of set $\level{k+1}$.  And for all
such newly discovered members of set $\level{k+1}$ we have that the
inductive hypothesis holds as we know that $\formula_{outer}$
holds. This is because in $\formula_{outer}$, for all functions that
replace the inner procedure call, there exists a function
$\F_{outer}$, which represents the return value.


%% We
%%   have that up-to the $n^{th}$ level there exists a function $\F'$ that
%%   satisfies $\formula_{merged}$. Next, because $\formula_{outer}$
%%   holds, we have $\formula_{merged}$ also holds up-to $\level{n+1}$ as
%%   $\formula_{merged}$ holds up-to $\level{n}$ and $\formula_{outer}$, so we
%%   can substitute $\F$ with $\F'$ up-to $\level{n}$. This also describes
%%   function $\F'$ for $\level{n}$.
  
Hence proved.
\end{proof}
  
\begin{lemma} \label{lemma:pairwise}
  Let $\inv$ be any formula, $\pathCondition$ be a path condition for
  procedure `\foo', \\$\OPCheckA \mi{(\inv, \pathCondition)}$ return
  formula $\pairFormula$ such that $\pairFormula$ is
  unsatisfiable, $\inv$ is satisfiable and $\OPCheckE \mi{(\inv,
    \pathCondition)}$ returns formula $\formula'$ then $\formula'$ is
  satisfiable.
\end{lemma}

\begin{proof}
  Let $X$ be the set of free variables in formula
  $\pairFormula$ except the mathematical function $\F$. We have
  $\pairFormula = \formula_1 \wedge \formula_2$. Formula
  $\formula_1$ and $\formula_2$ are listed above in Section
  \ref{opcheckBreakdown}.
  
  $\pairFormula$ is UNSAT; it is equivalent to say $\exists \F
  \exists(X) \pairFormula$ is false. Thus, we have $\forall \F
  \forall (X) \neg \pairFormula$ is true. Next, we have $\forall \F
  \forall (X) \neg \pairFormula = \forall \F \forall (X) \neg(
  \formula_1 \wedge \formula_2) = \forall \F \forall (X) \neg
  \formula_1 \vee \neg \formula_2 = \forall \F \forall (X) \formula_1
  \implies \neg \formula_2$. Let $\formula_3 = \forall \F \forall (X)
  \formula_1 \implies \neg \formula_2$.

  Next, we have the following properties :
  \begin{enumerate}
  \item $\formula_{pairwise}$ is unsatisfiable $\implies \formula_3$
    is valid (illustrated above). 
  \item Let $\forall \F \exists \F_{outer} \forall (X) \formula_1
  \implies (\neg \formula_2 \wedge \retVar_1 = \F_{outer}(\n_1))$ be
  formula $\formula_4$
    From lemma~\ref{lemma:forAllOuterAInner} we have $\formula_3 =
    \formula_4$.
  \item Let $\formula_{merged}$ be $\exists \F \forall (X) \formula_1
    \implies (\neg \formula_2 \wedge \retVar_1 = \F(\n_1))$. From
    lemma~\ref{lemma:combiningF} we have $\formula_4 \implies
    \formula_{merged}$. 
  \item Since formula $\formula_{merged}$ holds we have $\forall (X) \formula_1
    \implies (\neg \formula_2 \wedge \retVar_1 = \F(\n_1))$ is
    Satisfiable. Thus, $\formula'$ is satisfiable.
  \end{enumerate}
Hence proved.
%%   ,\\ \\ \\ \\ thus, $\forall \F \exists
%%   (X) \pairFormula$ is false. Let $\forall \F \exists (X)
%%   \formula = \neg\formula_3$. We have $\formula_3 = \exists \F
%%   \forall(X) \neg \formula = \exists \F \forall(X) \neg(\formula_1
%%   \wedge \formula_2) = \exists \F \forall(X) \neg \formula_1 \vee \neg
%%   \formula_2 = \exists \F \forall(X) \formula_1 \implies \neg
%%   \formula_2$.  We have $\formula_3$ is true since $\forall \F \exists
%%   (X) \formula$ is false. Let $\formula_4$ be $\exists \F \forall (X
%%   -{\n_1, \retVar_1}) \formula_1 \implies \formula_2$




%%   Formula $\formula_3$ is true thus we have $\forall(X)
%%   \formula_1 \implies \neg\formula_2$ is satisfiable. Let $\forall(X)
%%   \formula_1 \implies \neg \formula_2$ be $\formula_4$. And
%%   $\formula_4$ is satisfiable.

%% The following formula is $\formula_4$ : \\
%% $\forall \g_1 \forall \g_2 \forall \forall \gaft_{p1}
%% \forall \gaft_{p2} \cdots \forall \gaft_{q1} \forall \gaft_{q2}\cdots
%% \forall \n_2 \forall \n_1 \forall \retVar_1 \forall \gout_1 \forall
%% \gbef_{p1} \forall \gbef_{p2} 
%% \inv[\g_1/\g] \wedge \inv[\g_2/\g]
%% \wedge \inv[\gaft_{p1}/\g] \wedge \inv[\gaft_{p2}/\g] \cdots
%% \wedge \inv[\gaft_{q1}/\g] \wedge \inv[\gaft_{q2}/\g] \cdots
%% \wedge \n_1 = \n_2 
%% \wedge \pathCondition[\g_1/\g, \n_1/\n, \retVar_1/\retVar,
%% \gout_1/\gout, \gbef_{p1}/\gbef_1 \cdots , \gaft_{p1}/\gaft_1\cdots]\\
%% \wedge \pathCondition[\g_2/\g, \n_2/\n, \retVar_2/\retVar,
%% \gout_2/\gout, \gbef_{q1}/\gbef_1 \cdots , \gaft_{q1}/\gaft_1\cdots]
%% \\\implies
%%  (\retVar_1 = \retVar_2 \wedge \inv[\gbef_{p1}/\g] \wedge
%% \inv[\gbef_{p2}/\g] \cdots \wedge \inv[\gout/\g])$

%% Now let $\formula_5 = \forall(X) \formula_1[\F_{inner}/\F] \implies
%% (\neg \formula_2[\F_{inner}/\F] \wedge \retVar_1 =
%% \F_{outer}(n_1))$. We can construct a function $\F_{outer}$ that
%% satisfies $\formula_5$ by substituting values for variable $n_1$ in
%% formula $\formula_4$ and then mapping $n_1$ to $\retVar_1$ in function
%% $\F_{outer}$.

%% We will now prove the following obligations in order to prove this lemma:
%% \begin{enumerate}
%% \item \label{obligation1} if $\formula_5[\F'/\F_{outer}]$ is true then
%%   $\F'$ does not map an element of the domain to multiple elements in
%%   the range. Which in a necessary condition for $\F'$ to be a
%%   function.
%% \item \label{obligation2} formulae $\formula_5$ and $\formula_4$ are
%%   equisatisfiable.
%% \item \label{obligation3} if $\formula_5[\F'/\F]$ is true, then
%%   $\formula'[\F'/\F]$ is true.
%% \end{enumerate}
%% Proving obligation \ref{obligation1}:\\
%% This is a proof by contradiction. Let $\F'$ be a relation such that it
%% is a superset of $\{(d, v_1), (d, v_2)\}$ and $\formula_5[\F'/\F]$ be
%% true. Relation $\F'$ is not a function as it relates an element ($d$)
%% to two values $v_1$ and $v_2$. We know that $\F'$ was constructed by
%% substituting different values for $\n$ and getting values for
%% $\retVar_1$. And it is easy to see that if a values $(a, b)$ were
%% produced from a satisfying assignment to formula $\formula_4$ where
%% $a$ and $b$ are values for $\n$ and $\retVar_1$, then the value of $b$
%% can be substituted for $\retVar_2$ and the formula will still be true.
%% So we can substitute values $d$, $v_1$ and $v_2$ for variables
%% $\n$, $\retVar_1$ and $\retVar_2$ respectively. We have
%% $\formula_4[d/\n, v_1/\retVar_1, v_2/\retVar_2]$ is false as $v_1 \neq
%% v_2$ and also the premise is true as this is how values $v_1$ and
%% $v_2$ were generated.

%% maps value $d$ from the domain to values $v_1$ and $v_2$ and $v_1 \neq
%% v_2$, $\formula_5[\F'/\F]$ be true. In formula $\formula_5$ if we
%% substitute values $v_1$ and $v_2$ for variables $\retVar_1$ and
%% $\retVar_2$ respectively, formula $\neg\formula_2$ becomes
%% unsatisfiable as it now has the constraint $v_1 = v_2$ but $v_1 \neq
%% v_2$. We have $\inv$ is satisfiable thus there are some possible
%% values of the global variables that this formula allows. Now if
%% formula $\formula_1$ if unsatisfiable, it implies that procedure
%% $\foo$ does not have any traces that begin in the states that $\inv$
%% allows, which will make our claims vacusly true.

%% proving obligation \ref{obligation2}:\\








%% $\formula_3 \wedge \retVar_1 = \F(\n_1)$ and wee
%% can construct $\F$ by substituting different values $v$ for the variable
%% $\n_1$ and $k$ for $\retVar_1$ such that $\formula_3$ is
%% satisfied. The satisfying function $\F$ to formula $\formula_4$ would
%% map $v$ to $k$.

%% Now we will show that the satisfying assignment 
%% will
%% show that $\F$ is indeed a function such that if $\F \satisfies
%% \formula_4$ then $\F \satisfies \formula'$



%% We can
%% construct a function $\F$ by putting in values for each $\n_1$ and
%% mapping the result to $\retVar$. And by construction for any value of
%% $\g_1, \gaft_{p1}, \gaft_{p2} \cdots$ such that $(\g_1, \F) \satisfies
%% \inv$,  $(\gaft_{p1}, \F) \satisfies \inv$ and $(\gaft_{p2}, \F)
%% \satisfies \inv \cdots$ each value of $\n_1$ maps to a single return
%% value. Thus $\formula_2$ and $\formula_1$ are equisatisfiable. 

%% Now it is clear to see that $\formula_2$ and $\formula'$ are also
%% equisatisfiable, and the satisfying assignment for $\F$ in $\formula_2$
%% will also satisfy $\formula'$.
%% Thus $\formula'$ is SAT.

%% Hence Proved


  
  %% And let $\lbrace\inv[\globals{\state_1}/\g]
  %% \wedge \inv[\globals{\state_2}/\g] \wedge
  %% \param{\state_1} = \param{\state_2} \wedge
  %% \fooFormulaRec{\state_1}{\state_1'}{\inv} \wedge
  %% \fooFormulaRec{\state_2}{\state_2'}{\inv}\rbrace \implies
  %% (\return{\state_1'} = \return{\state_2'} =
  %% \F(\param{\state_1}))$ be $\formula_2$. Now $\formula_2$ is
  %% satisfiable as we can construct the satisfying assignment $\F$.

  %% We can build $\F$, mapping by mapping by assigning values to
  %% $\param{\state_1}$ which would give value for
  %% $\F(\param{\state_1})$ in $\formula_2$. And this satisfying must
  %% exist as $\formula_2$ is VALID.

  %% Now $\formula_2 \implies \formula'$, thus $\formula'$ is SAT.
\end{proof}

\begin{theorem}
  Let $\inv$ be an invariant, $T$ be a path condition,
  $\OPCheckA \mi{(\inv, T)}$ return formula $\formula$ such that
  $\formula$ is UNSAT, $\inv$ is SAT. Method `\foo' is
  observationally pure for all states $\state$ such that $\state
  \satisfies \inv$.
\end{theorem}

\begin{proof}
  We have $\OPCheckA \mi{(\inv, T)}$ for method `\foo' returns formula
  $\formula$ such that $\formula$ is UNSAT. From
  lemma~\ref{lemma:pairwise} we have that $\OPCheckE \mi{(\inv, T)}$
  would return formula $\formula'$ such that $\formula'$ is SAT, which
  implies that `\foo' is static observationally pure. Hence we
  can apply theorem~\ref{theorem:swopGivesWop}.\\
  Hence proved.
\end{proof}
  
\end{document}
