var cache: [int] int;
procedure {:entrypoint} fib(a: int) returns (r: int) modifies cache;{
	if( a <= 2) { r := 1;
	} else {
		if( cache[a] != 0) {
	       	   r := cache[a];
		} else {
			call r := fib(a -1) + fib( a - 2);
			cache[a] = r;
		}
	}
}
