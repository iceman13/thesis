\documentclass[../main.tex]{subfiles}
\begin{document}
\chapter{Algorithm} \label{cha:algo}
In this chapter we present the algorithms that check if the procedure
$\foo$ is weak observationally pure under invariant $\inv$. Here the
$\foo$ may have recursive calls to itself. We begin by contrasting the
path condition generation for recursive programs in comparison to path
condition generation for recursion free programs. Then we describe the
Observational purity checkers, which are very similar to the checkers
for recursion free procedures. Next we give routines to check if
invariant $\inv$ is inductive and also a procedure to build $\inv$
given a set of initial states.

\section{Path Condition Generation}
Path condition generation for procedures that have recursive calls is
very similar to path condition generation for recursion free
procedures as described in Section~\ref{sec:pcGenForRecFree}. The only
difference is the set of free variables. We add the variables $\gbef_i
\; and \; \gaft_i$ for the $i^{th}$ recursive call. $\gbef_i \; and \;
\gaft_i$ represent the value of latest version of $\g$ before and
after the $i^{th}$ recursive call respectively.

The reason we have these extra free variables is that
\begin{enumerate}
\item before each recursive call, the global state must be a good
  state.
\item after each recursive call, the global state must be a good
  state.
\end{enumerate}
Hence we have these extra free variables and they are used in the
invariant generation and inductivity checker.

\section{Observational purity Checkers}
In this section we present the checkers that extend ideas presented in
Chapter~\ref{cha:algoRecFree}, the only difference is that these
observational purity checkers can work with recursive calls as well.
Same as before both the checkers assume that the invariant $\inv$ is
inductive.

\subsection{Existential Checker}
The existential checker extends existential checker for recursion free
programs in Section ~\ref{sec:existentialCheckerRecFree} by assuming
that after returning from a recursive call the global state is a good
state. 
\begin{figure}[htp]
  \begin{algorithm}[H]
    \begin{align*}
      \OPCheckE & \mi{(\inv :
        invariant, \pathCondition : path \; condition)} \equiv \\
      & (\forall \g) (\forall \n) (\forall \retVar) (\forall \gout)\\
      & (\forall \gbef_1) (\forall \gbef_2) ....\\
      & (\forall \gaft_1) (\forall \gaft_2) ....\\
      & \{\inv \wedge \pathCondition\\
      & \wedge \inv[\gaft_1/\g] \wedge \inv[\gaft_2/\g] \cdots\\
      &\implies \retVar = \F(\n) \} \\
    \end{align*}
    \caption{Existential check : produces a formula whose
      satisfiability implies there is a consistent interpretation}
      \label{algo:someOPcheck}
  \end{algorithm}  
\end{figure}

\subsection{Impurity witness}
Again the generalized impurity witness extends the impurity witness
for recursion free program in Figure~\ref{algo:pairwiseOPcheck}, the
only difference is that it assumes that after returning from the
recursive call the global state is a good state.

\begin{figure}[htp]
  \begin{algorithm}[H]
    \begin{align*}
      \OPCheckA &\mi{(\inv : invariant,
        \pathCondition: path\; condition)} \equiv \\
      &\inv[\g_1/\g] \wedge \inv[\g_2/\g]\\
      & \wedge \inv[\gaft_{p1}/\g] \wedge \inv[\gaft_{p2}/\g] \cdots\\
      & \wedge \inv[\gaft_{q1}/\g] \wedge \inv[\gaft_{q2}/\g] \cdots\\
      &\wedge \n_1 = \n_2 \\
      &\wedge \pathCondition[\g_1/\g, \n_1/\n, \retVar_1/\retVar,
        \gout_1/\gout, \gbef_{p1}/\gbef_1 \cdots , \gaft_{p1}/\gaft_1\cdots]\\
      &\wedge \pathCondition[\g_2/\g, \n_2/\n, \retVar_2/\retVar,
        \gout_2/\gout, \gbef_{q1}/\gbef_1 \cdots , \gaft_{q1}/\gaft_1\cdots]\\
      &\wedge \retVar_1 \neq \retVar_2\\      
    \end{align*}
    \caption{Impurity witness : produces a formula whose unsatisfiability
    implies weak observational purity}
    \label{algo:pairwiseOPcheck}
  \end{algorithm}  
\end{figure}

\section{Inductivity Check}
The inductivity check in Algorithm~\ref{algo:inductivityCheck} returns
a formula that if valid implies that every execution of $\foo$ that
begins in a good state also ends in a good state. This formula assumes
that the value of $\g$ at the beginning of $\foo$ is $\g$ before the
method call is $\gbef$, after the method call is $\gaft$ and at the
end of foo is $\gout$. Then if $(\g, \F) \satisfies \inv$ and $(\gaft,
\F) \satisfies \inv$ then $(\gbef, \F) \satisfies \inv$ and $(\gout,
\F) \satisfies \inv$.

The result from Observational purity checkers is only guaranteed to be
correct if the invariant $\inv$ is inductive. 

\begin{figure}[htp]
  \begin{algorithm}[H]
    \begin{align*}
      \mi{Is\mhyphen inductive} &\mi{(\inv : invariant, \pathCondition :
        path \; condition)} \equiv \\
      &(\forall \g) (\forall \gout)\\
      &(\forall \gaft_1) (\forall \gaft_2) \cdots\\
      &(\forall \gbef_1) (\forall \gbef_2) \cdots \{\\
      &\inv \wedge \inv[\gaft_1/\g] \wedge \inv[\gaft_2/\g] \cdots \\
      &\wedge \pathCondition \implies\\
      &\inv[\gout/\g] \wedge \inv[\gbef_1/\g] \wedge \inv[\gbef_2/\g] \cdots\\
      & \} \\
    \end{align*}
    \caption{Is-inductive : is true when $\inv$ is an inductive
      invariant} 
    \label{algo:inductivityCheck}
  \end{algorithm}  
\end{figure}

\section{Combining both checks}
Combining both the checks makes the whole approach more precise.

\subsection{Existential}

The formula presented in Algorithm~\ref{algo:someOPcheckCombined}
combines the existential check and the inductivity check. The
inductivity check in Algorithm~\ref{algo:inductivityCheck} checks if
invariant $\inv$ is inductive for all functions. Here we combine the
two checks and this will allow an invariant $\inv$ which is not
inductive for all functions, but only for the satisfying function
$\F$. 

This allows
a more precise result as the inductivity check here is a SAT query
instead of a VALID query and the invariant need not be inductive for
all functions.

\begin{figure}[htp]
  \begin{algorithm}[H]
    \begin{align*}
      \OPCheckE_{combined} & \mi{(\inv :
        invariant, \pathCondition : path \; condition)} \equiv \\
      & (\forall \g) (\forall \n) (\forall \retVar) (\forall \gout)\\
      & (\forall \gbef_1) (\forall \gbef_2) \cdots\\
      & (\forall \gaft_1) (\forall \gaft_2) \cdots\\
      & \{\inv \wedge \pathCondition \wedge\\
      & \inv[\gaft_1/\g] \wedge \inv[\gaft_2/\g] \cdots\\
      &\implies (\retVar = \F(\n) \wedge\\
      & \inv[\gout/\g] \wedge \\
      & \inv[\gbef_1/\g] \wedge \inv[\gbef_2/\g] \cdots)\\
      & \}\\
    \end{align*}
    \caption{Existential check (combined) : produces a formula whose
      satisfiability implies there is a consistent interpretation and
      also ensures that the invariant is inductive for a function}
    \label{algo:someOPcheckCombined}
  \end{algorithm}  
\end{figure}

\subsection{Impurity Witness}

\begin{figure}[htp]
  \begin{algorithm}[H]
    \begin{align*}
      \OPCheckA &\mi{(\inv : invariant,
        \pathCondition: path\; condition)} \equiv \\
      &\inv[\g_1/\g] \wedge \inv[\g_2/\g]\\
      & \wedge \inv[\gaft_{p1}/\g] \wedge \inv[\gaft_{p2}/\g] \cdots\\
      & \wedge \inv[\gaft_{q1}/\g] \wedge \inv[\gaft_{q2}/\g] \cdots\\
      &\wedge \n_1 = \n_2 \\
      &\wedge \pathCondition[\g_1/\g, \n_1/\n, \retVar_1/\retVar,
        \gout_1/\gout, \gbef_{p1}/\gbef_1 \cdots , \gaft_{p1}/\gaft_1\cdots]\\
      &\wedge \pathCondition[\g_2/\g, \n_2/\n, \retVar_2/\retVar,
        \gout_2/\gout, \gbef_{q1}/\gbef_1 \cdots , \gaft_{q1}/\gaft_1\cdots]\\
      &\wedge (\retVar_1 \neq \retVar_2 \vee \neg\inv[\gbef_{p1}/\g] \vee
      \neg\inv[\gbef_{p2}/\g] \cdots \vee \neg\inv[\gout/\g])\\
    \end{align*}
    \caption{Impurity witness (combined): produces a formula whose unsatisfiability
    implies weak observational purity}
    \label{algo:pairwiseOPcheckCombined}
  \end{algorithm}  
\end{figure}


\section{Grow Invariant}\label{sec:growInvariant}
\begin{algorithm}[htp]
  \begin{algorithmic}
      \STATE Formula getInvariant( Formula $initialInv$, PathCondition
      $\pathCondition$) \{
      \STATE $inv = initialInv$;
      \WHILE{Not-VALID($Is \mhyphen inductive( inv, \pathCondition)$)}
      \STATE $k =  inv \wedge inv[\gaft_1] \wedge inv[\gaft_2] \cdots
      \wedge \pathCondition$;
      \STATE $inv = inv \vee (forall\; \gbef_1, \gbef_2
      \cdots,\gaft_1, \gaft_2 \cdots)k[\g/\gout] \vee
      (forall\; \gbef_2
      \cdots,\gaft_1, \gaft_2 \cdots, \gout)k[\g/\gbef_1] \vee
      (forall\; , \gbef_1
      \cdots,\gaft_1, \gaft_2 \cdots, \gout)k[\g/\gbef_2] \vee
      \cdots;$
      \ENDWHILE
      \RETURN $inv$;
  \end{algorithmic}
  \caption{ getInvariant($\formula$, $\pathCondition$) returns the
    invariant start form states in $\formula$, if it terminates}
\end{algorithm}

\end{document}
