var p: [int] int;
var m: [int, int] int;
procedure {:entrypoint} mcm(i: int, j: int) returns (r: int) modifies m;{
	var k, q : int;
	var a, b :int;
	if(i == j) {										
		m[i, j] := 0;
		r := 0;
	} else { 
		if( m[i, j] > 0) {							
			r := m[i, j];
		} else {								
			k := i;
			while(k < j) {							
				call a := mcm(i, k);
				call b := mcm(k+1, j);
				q := a + b + p[i-1] * p[k] * p[j];
				if( q < m[i, j]) {					
					m[i, j] := q;
				}
				k := k +1;
			}
			r := m[i, j];
		}
	}
}
