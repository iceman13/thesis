\documentclass[../thesis.tex]{subfiles}
\begin{document}
\chapter{Programming Language} \label{cha:pLanguage}

In this chapter we will describe the imperative programming language
for which our analysis is designed. Since our analysis does not work
with heap, we do not mention the constructs for heap access
in the programming language.
This thesis focuses on proving that a given procedure mimics a
mathematical function. For convenience we call this given procedure `\foo'.

For the purposes of this thesis we assume that this procedure's code
is based on the grammar in Figure~\ref{fig:grammar}.

\begin{figure}[phb!]
  %% \includegraphics[trim={0 5cm  3cm 0}, width=6cm]{grammar.eps}
  \begin{grammar}
    <statement> ::= <ident> = <constant>
    \alt <ident> = <logical-expr>
    \alt <ident> = <arithmetic-expr>
    \alt <statement>; <statement>
    %% \alt if<logical-expr> then <statement> else <statement>
    %% \alt while<logical-expr> do <statement>
    \alt <ident> = foo(<params-list>)
    \alt assume(<logical-expr>)
    \alt <ident> = \foo(<ident>)
    \alt havoc(<ident>)
    %% \alt <ident>[<ident>] = <ident>
    %% \alt <ident> = <ident>[<ident>]

    %% <params-list> ::= <params-list> , <ident>
    %% \alt <ident>

    <arithmetic-expr> ::= <ident> "+" <ident>
    \alt <ident> "-" <ident>
    \alt <ident> "/" <ident>
    \alt <ident> "*" <ident>
    \alt <ident> "\%" <ident>
 
    <logical-expr> ::= <ident> "<" <ident>
    \alt <ident> ">" <ident>
    \alt <ident> "==" <ident>
    \alt <ident> $\leq$ <ident>
    \alt <ident> $\geq$ <ident>
    \alt <logical-expr> $\wedge$ <logical-expr>
    \alt <logical-expr> $\vee$ <logical-expr>
    \alt $\neg$<logical-expr>
    \alt true
    \alt false

    <ident> ::= <local-ident>
    \alt <global-ident>
    
    <procedure> ::= procedure \foo ($\n$) {<statement>;}
  \end{grammar}
  \caption{Grammar for our programming language}
  \label{fig:grammar}
\end{figure}

\paragraph{} \label{topic:meaningAssume}
Most of the statements we allow in our programming language are
standard and do not need further explanation. For the rest explanation
follows.  The statement `havoc(x)' assigns some value from the domain
of variable `x' to `x'.  The `assume' statement gives a condition that must hold
at the program point just before this statement. The `assume'
statement can be used to represent conditional branches of a
program. The program statement `$\mi{if(x + 1 == 2)\; z = 3;\; else\;
  z = 2;}$' can be represented as `$\mi{assume(x + 1 == 2)\; z = 3}
\vee \mi{assume(x + 1 != 2) z = 2}$'.

Also we assume that the programming language does not have a return
statement. Instead, it has a variable `\retVar' which is assigned the
values that are to be returned.


We make some additional assumptions on the input program for the
simplicity of presentation of our routines in Chapter~\ref{cha:algo}.
\begin{enumerate}
\item Procedure `\foo' is assumed to have a single argument `n' and
  variable `n' is never written to inside procedure `\foo'. 
\item Procedure `\foo' is the only procedure in the program that we
  are analyzing. Also, the other procedures in the program do not write
  to the global variable that `\foo' refers to.
\item No variable in `\foo' is used before it is defined, except for
  global variables.
\item `\foo' may have recursive calls but no loops.
\item `\foo' has at most a single procedure call statement in any path
  from the beginning of procedure `\foo' till the end of `\foo'. The
  procedure call, if present, must be to `\foo' itself.
\item The return value from procedure calls are assigned only to local
  variables.
\end{enumerate}

In Chapter~\ref{cha:extensions} we discuss how this analysis can be
lifted to handle a procedure with multiple arguments, arrays and
loops. 

%% \section{Converting to expected input}\label{sec:toSSA}
\section{Converting the given program to SSA form}\label{sec:toSSA} 
\subsection{Static single assignment form}
Static single assignment (SSA) form is the property of an intermediate
representation, which requires that each variable is assigned only
once and every variable is defined before it is
used~\cite{cytron1991efficiently}. Since each variable can be assigned
only once, SSA conversion tools concatenate a version number
to each variable. Each assignment to a variable increments its
version number. 

\subsection{Conversion to SSA-like form}\label{sec:programStructure}
\subsubsection{Step 1 : conversion to our language}
Our programming language does not have return statements. Instead, it
has a special variable $\retVar$, and $\retVar$ is assigned the
return value. Also, a procedure call cannot be a part of an
expression. Thus an expression with a procedure call must be broken
into two statements. In the first statement, the procedure calls
returns the result to a temporary variable. Next, this temporary variable is used
in the expression. Listing~\ref{lst:factorial} presents an analogue
of procedure `factorial' in Listing~\ref{lst:factorialSimple} which is
the output from this step. Note that in line no (4 ,8, 10, 12) the
return statement has be replaced by an assignment statement, and in
line no.(6, 7), the expression with the procedure call is split into
two statements. 

\begin{lstlisting}[language=c, caption= {Procedure `factorial' :
      analogue of procedure `factorial' in
      Listing~\ref{lst:factorialSimple}}, label=lst:factorial] 
int g = -1;
int factorial( int n) {
  if(n <= 1) {
    retVar = 1;                    
  } else if(g == -1 && n == 19) {
    temp = factorial( 18 );
    g = 19 * temp;
    retVar = g;
  } else if(g != -1 && n == 19) {
    retVar = g;
  } else {
    retVar = n * factorial( n - 1 );
  }
}
\end{lstlisting}

\subsubsection{Step 2: Removing procedure call statements}

Next, we remove the recursive procedure
and model them using `havoc' statements. Each procedure call
statement `x = \foo(y)' is replaced with `havoc(x)'. A
procedure call statement may modify global variables, so we also add
`havoc(k)' for each global variable `k' after a procedure call
statement.

In Listing~\ref{lst:factorialRecFree} we show the outcome of performing
this step on procedure `factorial' in
Listing~\ref{lst:factorial}. The procedure call statement `temp =
factorial(18)' was changed to `havoc(temp)'(line no 6). Also the procedure call
statement `temp = factorial(18)' could have modified variable `g',
so we have added the statement `havoc(g)' (line no 7). Similarly, in
line no (13, 14).

\begin{lstlisting}[language=c, caption= {Procedure `factorial' from
      Listing~\ref{lst:factorial} converted to SSA-like form},
    label=lst:factorialRecFree] 
int g = -1;
int factorial( int n) {
  if(n <= 1) {
    retVar = 1;
  } else if(g == -1 && n == 19) {
    havoc(temp); // temp = factorial(18);
    havoc(g);  // g could have been modified in the procedure call
    g = 19 * temp;
    retVar = g;
  } else if(g != -1 && n == 19) {
    retVar = g;
  } else {
    havoc(temp1); // temp1 = factorial(n-1);
    havoc(g);  // g could have been modified in the procedure call
    retVar = n * temp1;
  }
}
\end{lstlisting}


\subsubsection{Step 3 : Conversion to SSA form}

Our approach requires the procedure being analyzed to be in SSA
form. Therefore, first we convert our code into SSA form. For instance
in Listing~\ref{lst:factorialSSA} we present the SSA version of
procedure `factorial' in Listing~\ref{lst:factorialRecFree}.

\begin{lstlisting}[language=c, caption= {Procedure `factorial' from
      Listing~\ref{lst:factorial} in SSA form}, label=lst:factorialSSA]
int g = -1;
int factorial( int n) {
  if(n <= 1) {
    retVar = 1;
  } else if(g == -1 && n == 19) {
    havoc(temp1);       // havoc(temp)
    havoc(g1);          // havoc(g)
    g2 = 19 * temp1;    // g1 = 19 * temp
    retVar = g2;
  } else if(g != -1 && n == 19) {
    retVar = g;
  } else {
    havoc(temp2);       //temp1 = factorial( n - 1 );
    havoc(g1);          // havoc(g)
    retVar = n * temp2;
  }
}
\end{lstlisting}

A `havoc' statement can be thought of as an assignment of a random
value. Thus in Listing~\ref{lst:factorialSSA} in line no. (6, 7 ,13
and 14), we use a new version of the respective variables. And for the
assignment statement in line no 8, we use a new version of variable
`g'. 

\subsubsection{Step 4 : Inserting function symbol and additional
  variables}\label{sec:pLanguageFinal}

Replacement of procedure calls with havoc statements as mentioned
above can introduce extra behaviors in the program, as the semantics
of the recursive call is lost. Therefore, to make up for this, we
insert back an expression in terms of the function symbol `$\F$' to
represent the original recursive function calls.

In procedure `factorial' in
Listing~\ref{lst:factorialSSAFinal}, we have inserted the statement
`assume(temp = F(18))' just after the statement `havoc(temp)' that we
had placed in the previous step. This step restricts the extra
behaviors we had added in the program in the previous step. Similarly,
for the procedure call statement `temp1 = factorial(n-1)'.

Next we add some extra variables the the program. First, we number the
procedure call statements in the program. We then add variables
`gbefi' and `gafti', before and after the i$^{th}$ procedure call
statement. These variable are assigned the current versions of the
global variable `g'. Next, we introduce a single variable `gout' to
represent the value of `g' at the end of the procedure. In
Listing~\ref{lst:factorialSSAFinal} we can see these variables added to
the program.
\begin{lstlisting}[language=c, caption= {Procedure `factorial' from
      Listing~\ref{lst:factorialSSA} converted to the form our
      approach expects}, label=lst:factorialSSAFinal]
int g = -1;
int factorial( int n) {
  if(n <= 1) {
    retVar = 1;
    gout = g;
  } else if(g == -1 && n == 19) {
    gbef1 = g;
    havoc(temp1); 
    havoc(g1);
    assume(temp1 = F(18));  // newly added statement
    gaft1 = g1;
    g2 = 19 * temp1;
    retVar = g2;
    gout = g2;
  } else if(g != -1 && n == 19) {
    retVar = g;
    gout = g;
  } else {
    gbef2 = g;
    havoc(temp2);
    havoc(g1);
    assume(temp2 = F( n - 1 ));  //newly added statement
    gaft2 = g1;
    retVar = n * temp2;
    gout = g1;
  }
}
\end{lstlisting}

Now procedure `factorial' in Listing~\ref{lst:factorialSSAFinal} is in
a form that is suited to our analysis. Here onward we refer to
procedure `\foo' as a procedure that is suited for our analysis (the
final transformed procedure) and
procedure `$\fooO$' as its original analogue (with the procedure call
statements but no return statements). For instance procedure `factorial' in
Listing~\ref{lst:factorialSSAFinal} is in a form we expect procedure
`\foo' to be in, whereas Listing~\ref{lst:factorial} gives
procedure `factorial' which serves as an example of what procedure
`$\fooO$' looks like. 
\end{document}
